@extends('layouts.app_admin')
@section('title', 'Question Set')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Question Set</h1>
                </div>


                <div class="col-6 d-flex flex-row-reverse">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                        + Add New
                    </button>
                </div>
            </div>


            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            {{-- <th>Mark</th>
                                            <th>Duration (Minute)</th> --}}
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($questionSets as $key => $questionSet)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $questionSet->title }}</td>
                                                {{-- <td>{{ $questionSet->total_mark }}</td>
                                                <td>{{ $questionSet->duration_time }}</td> --}}


                                                <td>

                                                    <a class="btn btn-primary edit"
                                                        onclick="changeModalData( '{{ $questionSet }}')"
                                                        data-toggle="modal" data-target="#modal-part2">
                                                        Edit
                                                    </a>

                                                    <form method="POST"
                                                        action="{{ route('question-set.destroy', $questionSet->id) }}"
                                                        style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class=" btn-sm btn-danger"
                                                            onclick="return confirm('Confirm delete?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                        </button>
                                                    </form>

                                                    <a class="btn btn-info"
                                                        href="{{ route('questions.listOfQuestions', $questionSet->id) }}">
                                                        Questions
                                                    </a>

                                                    <a class="btn btn-primary edit"
                                                        href="{{ route('questions.uploadQuestion', $questionSet->id) }}">
                                                        Upload Question
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
        {{-- ADD Modal --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
            <div class="modal-dialog" role="document">
                {{--  --}}
                <form method="POST" action="{{ route('question-set.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Question Set</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Title <code>*</code></label>
                                <input type="text" name="title" class="form-control form-control-lg" />
                            </div>
                            {{-- <div class="form-group">
                                <label>Total Mark <code>*</code></label>
                                <input type="number" name="mark" class="form-control form-control-lg" />
                            </div>
                            <div class="form-group">
                                <label>Duration (Minute)<code>*</code></label>
                                <input type="number" name="duration" class="form-control form-control-lg" />
                            </div> --}}

                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">

                <form method="POST" action="{{ route('question-set.updateQuestionSet') }}">
                    {{ method_field('POST') }}
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Update Question Set</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Title <code>*</code></label>
                                    <input type="text" name="title" id="title" class="form-control form-control-lg" />
                                </div>
                                {{-- <div class="form-group">
                                    <label>Total Mark <code>*</code></label>
                                    <input type="number" name="mark" id="mark" class="form-control form-control-lg" />
                                </div>
                                <div class="form-group">
                                    <label>Duration (Minute)<code>*</code></label>
                                    <input type="number" name="duration" id="duration"
                                        class="form-control form-control-lg" />
                                </div> --}}

                            </div>
                            <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                        </div>

                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" id="editProductHead" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('extra-js')
    <script>
        function changeModalData(questionSet) {
            console.log(questionSet);

            questionSet_data = JSON.parse(questionSet);

            document.getElementById("id").value = questionSet_data.id;
            document.getElementById("title").value = questionSet_data.title;
            // document.getElementById("mark").value = questionSet_data.total_mark;
            // document.getElementById("duration").value = questionSet_data.duration_time;


        }
    </script>
@endsection
