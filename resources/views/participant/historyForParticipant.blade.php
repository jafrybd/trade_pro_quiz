@extends('layouts.app')
@section('title', 'Question Set')
@push('css')


@endpush

@section('content')
<link rel="stylesheet" src="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" />

<div class="thim-block-plugin-home" id="practice">
    <div class="container-content">
        <div class="row-content">
            <div class="col-lg-4 content-text">
                <h3 class="title-plugin wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
                    Test History
                </h3>
                @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                @endif

                @include('flash-message')
            </div>
            <div class="col-lg-8 content-img">
                <div class="table-responsive">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Question Set</th>
                                <th>Exam Date</th>
                                <th>Mark</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($participant as $key => $participantData)
                            <tr>
                                <td>{{ $key + 1 }}</td>

                                <td>
                                    {{ $participantData->questionSet->title }}
                                </td>

                                <td>
                                    {{ \Carbon\Carbon::parse($participantData->created_at)->format('d/m/Y') }}
                                </td>

                                <td>
                                    {{ $participantData->obtained_mark }}
                                </td>

                                <td>
                                    <span class="statuss">
                                        {{ $participantData->status }}
                                    </span>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @endsection

        @section('extra-js')
        <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js">

        </script>
        <script>
            $(document).ready(function() {
                $('#myTable').DataTable();
            });
        </script>
        @endsection