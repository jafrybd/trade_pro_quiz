@extends('layouts.app_admin')
@section('title', 'Question Set')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Participant Details</h1>
            </div>

        </div>


        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">

            <div class="col-12">
                <!-- <div class="row"> -->
                <div class="card">
                    <div class="card-body">
                        <form>
                            <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-first-name">First
                                                name</label>
                                            <input type="text" id="input-first-name" class="form-control form-control-alternative" value="{{ $participant->first_name }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-last-name">Last
                                                name</label>
                                            <input type="text" id="input-last-name" class="form-control form-control-alternative" value="{{ $participant->last_name }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-username">Contact
                                                Number</label>
                                            <input type="text" id="input-username" class="form-control form-control-alternative" value="{{ $participant->phone }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Email
                                                address</label>
                                            <input type="email" id="input-email" class="form-control form-control-alternative" value="{{ $participant->email }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <hr class="my-4"> -->
                            <!-- Address -->

                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-address">Address</label>
                                            <input id="input-address" class="form-control form-control-alternative" type="text" value="{{ $participant->address }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-city">City</label>
                                            <input type="text" id="input-city" class="form-control form-control-alternative" value="{{ $participant->city }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-country">State</label>
                                            <input type="text" id="input-country" class="form-control form-control-alternative" value="{{ $participant->state }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-country">Postal
                                                code</label>
                                            <input type="text" id="input-postal-code" class="form-control form-control-alternative" value="{{ $participant->postal_code }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Description -->

                        </form>
                    </div>
                </div>
                <!-- </div> -->
                <!-- </div> -->
            </div>
        </div>

    </section>



</div>

@endsection

@section('extra-js')
<script>

</script>
@endsection