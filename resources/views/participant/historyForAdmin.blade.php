@extends('layouts.app_admin')
@section('title', 'Question Set')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1> Test History of    {{ $participant_name }}</h1>
                </div>

            </div>


            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            
                                            <th>Question Set</th>
                                            <th>Exam Date</th>
                                            <th>Mark</th>
                                            <th>Status</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($participant as $key => $participantData)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                
                                                <td>

                                                    {{ $participantData->questionSet->title }}
                                                </td>

                                                <td>

                                                    {{ \Carbon\Carbon::parse($participantData->created_at)->format('d/m/Y') }}
                                                </td>

                                                <td>

                                                    {{ $participantData->obtained_mark }}
                                                </td>

                                                <td>
                                                    <span class="badge badge-info">
                                                        {{ $participantData->status }}
                                                    </span>

                                                    
                                                    
                                                </td>



                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>



    </div>

@endsection

@section('extra-js')
    <script>

    </script>
@endsection
