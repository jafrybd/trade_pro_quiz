@extends('layouts.app')
@section('title', 'Question Set')
@section('extra-css')
<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Concert+One|Nunito" rel="stylesheet">
<!-- <link rel="stylesheet" type="text/css" href="final.css"> -->
<style>
  /* h1 {
            margin: 60px 60px 0 60px;
            font-size: 30px;
            padding-bottom: 30px;
            border-bottom: 1px solid #b5b5b7;
            color: #ffcc00;
        } */

  /* h1, */
  /* h2 {
            text-align: center;
            font-weight: bold;
            letter-spacing: 10px;
            text-transform: uppercase;
        }

        h2 {
            font-size: 20px;
        } */
  button {
    margin-top: 20px;
    width: 200px;
    height: 40px;
    background: #c72424;
    font-family: 'Dosis', sans-serif;
    font-size: 30px;
    color: #ff4040;
    border: .5px solid #c72424;
  }

  .start-quiz {
    width: 500px;
    height: 300px;
    margin: 100px auto;
    /* text-align: center; */
    padding-bottom: 200px;
    color: white;
  }

  .cp {
    display: none;
  }

  .questions,
  .end-quiz {
    display: none;
    text-align: center;
    padding-bottom: 200px;
    margin: 100px auto 0 auto;
    text-align: center;
    width: 500px;
    height: 300px;
    margin-bottom: 100px;
  }

  .end-quiz {
    border: 2px solid #c72424;
    padding-top: 80px;
    height: 100px;
    font-size: 30px;
    color: #c72424;
  }

  .question-number,
  .score {
    font-size: 30px;
    color: #b5b5b7;
  }

  .retake-button {
    min-height: 60px;
    color: white;
  }

  ul {
    display: flex;
    width: 100%;
    list-style: none;
    padding: 0;
  }

  .score {
    font-family: Helvetica, Arial, sans-serif;
    display: inline-block;
    max-width: 100%;
    font-weight: 700;
    font-size: 16px;
    line-height: 23px;
    color: #333;
  }

  .list li {
    border: 1px solid #d6d6cf;
    width: 44%;
    margin: 15px 15px;
    border-radius: 5px;
    padding: 15px;
    text-align: center;
    height: auto;
    color: #000;
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;
    font-family: Helvetica, Arial, sans-serif;
    font-size: 16px;
    line-height: 23px;
  }

  .start-button {
    color: white;
  }


  .selected {
    background: #f2b632;
    color: #252839;
  }

  .correct {
    background-color: green;
    border: 1px solid green !important;
    color: white !important;
    padding: 10px 20px !important;
    margin: 10px !important;
  }

  .incorrect {
    background-color: red;
    border: 1px solid red !important;
    color: white !important;
    padding: 10px 20px !important;
    margin: 10px !important;
  }

  .dot {
    height: 300px;
    width: 300px;
    border-radius: 50%;
    display: inline-block;
  }

  #about {
    margin-top: 100px;
  }

  .comment {
    color: black;
  }

  .end-score {
    color: black;
  }
</style>

@endsection

@section('content')

<div class="thim-block-plugin-home ct" id="practice">
  <div class="container-content">
    <div class="row-content">
      <div class="col-lg-4 content-text">
        <h3 class="title-plugin wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
          Practice Test - <br> "{{ $questionSet->title }}"
        </h3>
        <!-- <p class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          Engage your audience in a unique and fun way and connect them to
          your brand or learning material.
        </p> -->
        <a href="{{ route('participant.examMaterial', $questionSet->id) }}" class="">
          <button class="btn btn-success" type="button"><span>Educational Material &nbsp; &#xbb;</span></button>
        </a>
      </div>
      <div class="col-lg-8 content-img">


        @foreach ($questionSubCategoryList as $key => $questionSet)

        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          @if(count($questionSet->questions) > 0)
          <a href="{{ route('participant.questions', $questionSet->id) }}" class="">
            <div class="content-img-plugin">
              <h2>
                <!-- <a target="_blank" rel="noopener noreferrer"> -->
                  <strong>{{ $questionSet->title }}</strong>
                <!-- </a> -->
              </h2>
              <div class="hover-img-plugin">
                <img class="btImg" src="{{ asset('../frontend/assets/img/redseal.png') }}" title="Eduma Demos" />
              </div>
              
              <h3>

                <strong>Start Test</strong>
              </h3>
            </div>
          </a>
              @else
              <a href="" class="">
                <div class="content-img-plugin">
                  <h2>
                    <!-- <a target="_blank" rel="noopener noreferrer"> -->
                      <strong>{{ $questionSet->title }}</strong>
                    <!-- </a> -->
                  </h2>
                  <div class="hover-img-plugin">
                    <img class="btImg" src="{{ asset('../frontend/assets/img/redseal.png') }}" title="Eduma Demos" />
                  </div>
                  
                  <h3>
    
                    <strong>Not Available</strong>
                  </h3>
                </div>
              </a>
              @endif
            </div>
          
        

        @endforeach


      </div>
    </div>
  </div>
</div>


@endsection

@section('extra-js')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="final.js"></script>
@endsection