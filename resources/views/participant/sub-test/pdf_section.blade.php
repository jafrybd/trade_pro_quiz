@extends('layouts.app')
@section('title', 'Question Set')
@section('content')
<div class="thim-block-plugin-home ct sub-part" id="practice">
  <div class="container-content">
    <div class="row-content">
      <div class="col-lg-4 content-text">
        <h3 class="title-plugin wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
        Educational Material on - "{{ $questionSet->title }}"
        </h3>
      </div>
      <div class="col-lg-8 content-img educationMetarial">
        
        {{--  PDF 1 --}}

        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Basic Instrumentation Measuring Devices And Basic PID Control</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/BASIC INSTRUMENTATION MEASURING DEVICES AND BASIC PID CONTROL.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/BASIC INSTRUMENTATION MEASURING DEVICES AND BASIC PID CONTROL.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>

        {{--  PDF 2 --}}

        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Basics of Control Valve Positioners - Learning Instrumentation And Control Engineering</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/Basics of Control Valve Positioners _ Learning Instrumentation And Control Engineering.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/Basics of Control Valve Positioners _ Learning Instrumentation And Control Engineering') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>

        {{--  PDF 3 --}}

        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Control Valve and positioner</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/Control Valve and positioner.docx') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/Control Valve and positioner.docx') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>

    {{--  PDF 4 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Electric Symbols INDESYM</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/electric symbols INDESYM.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/electric symbols INDESYM.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>

       {{--  PDF 5 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Emerson Control Valve-cvh99</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/EMERSON CONTROL VALVE-cvh99.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/EMERSON CONTROL VALVE-cvh99.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>



        {{--  PDF 6 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>How a Current to Pressure Transducer (I_P) Works - Learning Instrumentation And Control Engineering</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/How a Current to Pressure Transducer (I_P) Works _ Learning Instrumentation And Control Engineering.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/How a Current to Pressure Transducer (I_P) Works _ Learning Instrumentation And Control Engineering.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>



        {{--  PDF 7--}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>How a Solenoid Valve Works - Learning Instrumentation And Control Engineering</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/How a Solenoid Valve Works _ Learning Instrumentation And Control Engineering.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/How a Solenoid Valve Works _ Learning Instrumentation And Control Engineering.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>




        {{--  PDF 8 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>IDC  Industrial Automation_pocketbook_A6-rev2.2.doc</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/IDC  Industrial Automation_pocketbook_A6-rev2.2.doc.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/IDC  Industrial Automation_pocketbook_A6-rev2.2.doc.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>



        {{--  PDF 9 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>IDC FORMULAS & UNITS PG5_Formulas_r6</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/IDC FORMULAS & UNITS PG5_Formulas_r6.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/IDC FORMULAS & UNITS PG5_Formulas_r6.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>



        {{--  PDF 10 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>IDC Instrumentation-Scada and Telemetery System</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/IDC Instrumentation-Scada and telemetery system.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/IDC Instrumentation-Scada and telemetery system.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>



        {{--  PDF 11 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>ISA_5.1-2009 instrument</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/ISA_5.1-2009 instrument.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/ISA_5.1-2009 instrument.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>


        {{--  PDF 12 --}}

         <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          <div class="content-img-plugin">
            <h2>
            <a target="_blank" rel="noopener noreferrer"><strong>Process Control Fundamentals book by PA control</strong></a>
            </h2>
            <div class="hover-img-plugin">
              <div class="btn-scroll">
                <a href="{{ asset('../pdf/Process Control Fundamentals book by PA control.pdf') }}" download class="btn btn-primary w-100">
                  <img src="{{ asset('../frontend/assets/img/pdf.png') }}" >
                </a>
                
              </div>
            </div>
            
            <a href="{{ asset('../pdf/Process Control Fundamentals book by PA control.pdf') }}" download class="btn btn-primary w-100">
              <i class="fas fa-file-download"></i> Download
            </a>
          </div>
        </div>
        
        
        
        
        
        
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('extra-js')
@endsection