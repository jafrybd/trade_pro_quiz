<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('layouts.partial.web.head')
    @yield('extra-css')
</head>


<body class="animsition" data-new-gr-c-s-check-loaded="8.881.0" data-gr-ext-installed="" style="animation-duration: 1500ms; opacity: 1" cz-shortcut-listen="true">
    <div id="wrapper-container">

        @include('layouts.partial.web.nav')


        @yield('content')



        <footer>
            @include('layouts.partial.web.footer')
        </footer>

        @yield('extra-js');

    </div>

</body>

</html>