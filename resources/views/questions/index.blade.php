@extends('layouts.app_admin')
@section('title', 'Question Set')
@push('css')


@endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Questions of-> {{ $questionSet->title }}</h1>
                </div>


                <div class="col-6 d-flex flex-row-reverse">
                    <a class="btn btn-primary" href="{{ route('questions.createQuestion', $questionSet->id) }}">
                        +Add New
                    </a>
                </div>
            </div>


            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question (Text)</th>
                                            <th>Question (Image)</th>
                                            <th width="300px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($questions as $key => $question)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>
                                                    @if ($question->question_text != null)
                                                        {{ $question->question_text }}

                                                    @endif
                                                </td>
                                                <td >

                                                    @if ($question->question_image != null)
                                                        
                                                        <img alt="image" border=3 height="100px" width="150px" src="{{ asset('images/questions/' . $question->question_image) }}" />

                                                    @endif
                                                </td>

                                                <td>

                                                    <a class="btn btn-info"
                                                        href="{{ route('questions.show', $question->id) }}">
                                                        Details
                                                    </a>

                                                    <a class="btn btn-primary edit"
                                                        href="{{ route('questions.edit', $question->id) }}">
                                                        Edit
                                                    </a>


                                                    <form method="POST"
                                                        action="{{ route('questions.destroy', $question->id) }}"
                                                        style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class=" btn-sm btn-danger"
                                                            onclick="return confirm('Confirm delete?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                        </button>
                                                    </form>
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>



    </div>

@endsection

@section('extra-js')
    <script>

    </script>
@endsection
