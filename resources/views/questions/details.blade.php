@extends('layouts.app_admin')
@section('title', 'Question')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Question Details</h1>
            </div>

        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    {{-- {{ route('product.store') }} --}}
                    <div class="row card-body">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="form-group px-3">
                                    <!-- <label>Question (Text)</label> -->
                                    <!-- <input type="text" name="question_text" class="form-control form-control-lg"
                                    value="{{ $question->question_text }}" readonly /> -->
                                    <h2>{{ $question->question_text }}</h2>
                                </div>

                                <div class="form-group">
                                    <!-- <label>Question (Image)</label> -->
                                    @if ($question->question_image != null)
                                    <img src="{{ asset('images/questions/' . $question->question_image) }}" width="300" height="300" alt="user photo" style="padding-left: 0px;" /><br>

                                    @else No Image File
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 1 (Text)</label>
                                        <!-- <input type="text" name="option_1_text" class="form-control form-control-lg" value="{{ $question->option_1_text }}" readonly /> -->
                                        <span class="form-control mb-2 customHeight"> {{ $question->option_1_text }}</span>
                                    </div>
                                </div>
                                <div class="col-6">

                                    <div class="form-group">
                                        <label>Option 1 (Image)</label><br>
                                        @if ($question->option_1_image != null)
                                        <img src="{{ asset('images/questions/' . $question->option_1_image) }}" width="150px" height="150px" alt="user photo" style="padding-left: 0px;" /><br>

                                        @else No Image File
                                        @endif


                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 2 (Text)</label>
                                        <span class="form-control mb-2 customHeight">{{ $question->option_2_text }}</span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 2 (Image)</label><br>
                                        @if ($question->option_2_image != null)
                                        <img src="{{ asset('images/questions/' . $question->option_2_image) }}" width="100" height="100" alt="user photo" style="padding-left: 0px;" /><br>
                                        @else No Image File
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 3 (Text)</label>
                                        <span class="form-control mb-2 customHeight">{{ $question->option_3_text }}</span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 3 (Image)</label><br>
                                        @if ($question->option_3_image != null)
                                        <img src="{{ asset('images/questions/' . $question->option_3_image) }}" width="100" height="100" alt="user photo" style="padding-left: 0px;" /><br>
                                        @else No Image File
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 4 (Text)</label>
                                        <span class="form-control mb-2 customHeight">{{ $question->option_4_text }}</span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Option 4 (Image)</label><br>
                                        @if ($question->option_4_image != null)
                                        <img src="{{ asset('images/questions/' . $question->option_4_image) }}" width="100" height="100" alt="user photo" style="padding-left: 0px;" /><br>

                                        @else No Image File
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Correct Answer</label>
                                <input type="number" name="correct_answer" class="form-control form-control-lg" value="{{ $question->correct_answer }}" readonly />
                            </div>


                            <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly value="{{ $id }}" />

                            <input type="hidden" name="question_set_id" id="question_set_id" class="form-control form-control-lg" readonly value="{{ $question->question_set_id }}" />


                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- End -->
</div>



@endsection

@section('extra-js')


@endsection