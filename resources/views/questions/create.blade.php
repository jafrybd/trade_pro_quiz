@extends('layouts.app_admin')
@section('title', 'Question')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1> Add Question</h1>
                </div>

            </div>
            @if ($errors->any())
            <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        {{-- {{ route('product.store') }} --}}
                        <div class="card-body">
                            <form method="POST" action="{{ route('questions.store') }}" id="editForm" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Question (Text) <code></code></label>
                                    <input type="text" name="question_text" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Question (Image) <code></code></label>
                                    <input type="file" name="question_image" class="form-control form-control-lg" />
                                </div>


                                <div class="form-group">
                                    <label>Option 1 (Text) <code></code></label>
                                    <input type="text" name="option_1_text" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 1 (Image) <code></code></label>
                                    <input type="file" name="option_1_image" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 2 (Text) <code></code></label>
                                    <input type="text" name="option_2_text" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 2 (Image) <code></code></label>
                                    <input type="file" name="option_2_image" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 3 (Text) <code></code></label>
                                    <input type="text" name="option_3_text" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 3 (Image) <code></code></label>
                                    <input type="file" name="option_3_image" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 4 (Text) <code></code></label>
                                    <input type="text" name="option_4_text" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Option 4 (Image) <code></code></label>
                                    <input type="file" name="option_4_image" class="form-control form-control-lg" />
                                </div>

                                <div class="form-group">
                                    <label>Correct Answer <code></code></label>
                                    <input type="number" name="correct_answer" class="form-control form-control-lg" />
                                </div>

                               
                                <input type="hidden" name="question_set_id" id="question_set_id" class="form-control form-control-lg" readonly value="{{ $question_set_id }}"/>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary mr-1" type="submit">Submit</button>

                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <!-- End -->
    </div>



@endsection

@section('extra-js')

@endsection

