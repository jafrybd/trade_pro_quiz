<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
    }

    //protected $redirectTo;
    public function redirectTo()
    {
        
        switch(Auth::user()->role_id){
            case 1:
            $this->redirectTo = '/dashboard';
            return $this->redirectTo;
                break;
            case 2:
                    $this->redirectTo = '/';
                return $this->redirectTo;
                break;
            
            default:
                $this->redirectTo = '/login';
                return $this->redirectTo;
        }
         
        // return $next($request);
    }
    
    
    public function changePasswordUI()
    {
        if(Auth::user()->role_id == 1){
            return view('auth.passwords.admin_change_password');
        }
        else{
            return view('auth.passwords.participant_change_password');
        }
        
       
    }

    
    public function changePasswordConfirm(Request $request)
    {       
        $user = Auth::user();
    
        $userPassword = $user->password;
        
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|same:confirm_password|min:8',
            'confirm_password' => 'required',
        ]);

        if (!Hash::check($request->current_password, $userPassword)) {
            return back()->withErrors(['current_password'=>'Current password not matched']);
        }

        $user->password = Hash::make($request->password);

        $user->save();

        if(Auth::user()->role_id == 1){
            return redirect()->route('dashboard')->with('success', 'Password has been updated successfully');
        }
        else{
            return redirect()->route('changePasswordUI')->with('success', 'Password has been updated successfully');
        }

        
    }
    
}
