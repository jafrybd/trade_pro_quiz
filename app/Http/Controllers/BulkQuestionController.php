<?php

namespace App\Http\Controllers;

use App\Models\Questions;
use App\Models\QuestionSet;
use App\Http\Controllers\CommonController;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Session;
use Excel;
use File;
use Importer;
use Validator;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;

use function PHPUnit\Framework\isNull;

class BulkQuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadQuestion($question_set_id)
    {
        //

        $questionSet = QuestionSet::findorFail($question_set_id);

        return view('questions.bulkUpload', compact('question_set_id', 'questionSet'));
    }

    // public function importQuestions(Request $request)
    // {
    //     //
    //     dd("hi");

    //     $questionSet = QuestionSet::findorFail($question_set_id);

    //     return view('questions.bulkUpload', compact('question_set_id','questionSet')); 

    // }

    public function importQuestions(Request $request)
    {

        // validate the xls file
        $request->validate([
            'upload_question' => 'required|mimes:xlsx,xls',
            'question_set_id' => 'required',
        ]);
        
        $savePath = public_path('/uploads/bulk_question_file/'); //Path of picture local storage
        $imageFilePath = public_path('/images/questions/'); //Path of picture local storage

        if (!file_exists($savePath)) { //Recursively create if directory does not exist
            mkdir($savePath, 0777, true);
        }

        if (!file_exists($imageFilePath)) { //Recursively create if directory does not exist
            mkdir($imageFilePath, 0777, true);
        }

        $dateTime = date('Ymd_His');
        $file = $request->file('upload_question');
        $fileName = $dateTime . '-' . $file->getClientOriginalName();
        $file->move($savePath, $fileName);


        try {
            $inputFileName = $savePath . "/" . $fileName;  //Excel file containing pictures
            $objRead = IOFactory::createReader('Xlsx');
            $objSpreadsheet = $objRead->load($inputFileName);
            $objWorksheet = $objSpreadsheet->getSheet(0);
            $data = $objWorksheet->toArray();

            // dump(count($data));

            // dd($data);

            foreach ($objWorksheet->getDrawingCollection() as $drawing) {
                list($startColumn, $startRow) = Coordinate::coordinateFromString($drawing->getCoordinates());
                $imageFileName = $drawing->getCoordinates() . mt_rand(1000, 9999);

                switch ($drawing->getExtension()) {
                    case 'jpg':
                    case 'jpeg':
                        $imageFileName .= '.jpg';
                        $source = imagecreatefromjpeg($drawing->getPath());
                        imagejpeg($source, $imageFilePath . $imageFileName);
                        break;
                    case 'gif':
                        $imageFileName .= '.gif';
                        $source = imagecreatefromgif($drawing->getPath());
                        imagegif($source, $imageFilePath . $imageFileName);
                        break;
                    case 'png':
                        $imageFileName .= '.png';
                        $source = imagecreatefrompng($drawing->getPath());
                        imagepng($source, $imageFilePath . $imageFileName, 9);
                        break;
                }
                $startColumn = $this->ABC2decimal($startColumn);
                $data[$startRow - 1][$startColumn] = $imageFileName;
            }

            // dump(count($data));

            for ($i = count($data) - 1; $i > 0; $i--) {
                if ($data[$i][0] == null and $data[$i][1] == null) {
                    unset($data[$i]);
                }
            }

            // dump(count($data));
            // dump($data);


            // Save Start

            $question_set_id = $request->question_set_id;

            for ($i = 1; $i < count($data); $i++) {

                $question = new Questions();
                $question->question_set_id  = $question_set_id;
                $question->question_text = $data[$i][0];

                if ($data[$i][1] != " " || $data[$i][1] == "" || !isNull($data[$i][1])) {
                    $question->question_image = $data[$i][1];
                }


                $question->option_1_text =  $data[$i][2];
                if ($data[$i][3] != " " || $data[$i][3] == "" || !isNull($data[$i][3])) {
                    $question->option_1_image = $data[$i][3];
                }



                $question->option_2_text =  $data[$i][4];
                if ($data[$i][5] != " " || $data[$i][5] == "" || !isNull($data[$i][5])) {
                    $question->option_2_image = $data[$i][5];
                }


                $question->option_3_text =  $data[$i][6];
                if ($data[$i][7] != " " || $data[$i][7] == "" || !isNull($data[$i][7])) {
                    $question->option_3_image = $data[$i][7];
                }

                $question->option_4_text =  $data[$i][8];
                if ($data[$i][9] != " " || $data[$i][9] == "" || !isNull($data[$i][9])) {
                    $question->option_4_image = $data[$i][9];
                }

                $question->correct_answer =  $data[$i][10];
                $question->created_by = Auth::user()->id;
                $question->updated_by = Auth::user()->id;

                $question->save();
            }
            // Save end

            return redirect()->route('questions.uploadQuestion',compact('question_set_id'))->with('success', 'Question successfully updated');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function ABC2decimal($abc)
    {
        $ten = 0;
        $len = strlen($abc);
        for ($i = 1; $i <= $len; $i++) {
            $char = substr($abc, 0 - $i, 1); //Get single character in reverse

            $int = ord($char);
            $ten += ($int - 65) * pow(26, $i - 1);
        }
        return $ten;
    }
}
