<?php

namespace App\Http\Controllers;

use App\Models\QuestionSet;
use App\Models\Questions;
use App\Models\ParticipantHistory;
use App\Models\ParticipantComment;
use App\Models\User;
use App\Http\Controllers\CommonController;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ParticipantController extends Controller
{
    //
    public function index()
    {
        // $questionSets = QuestionSet::where('status', 1)->get();
       
        // return view('participant.participant', compact('questionSets'));
    }
    

    public function participantList()
    {
        $participants = User::where([['role_id', '=', 2], ['status', '=', 1]])->get();
         return view('participant.participantList', compact('participants'));
    }

    
    public function profileDetailsForAdmin($participant_id)
    {
        
        $participant = User::where([['role_id', '=', 2], ['id', '=', $participant_id]])->first();
         return view('participant.profileDetailsForAdmin', compact('participant'));
    }

    public function profile()
    {
        
        $participant = Auth::user();
         return view('participant.profile', compact('participant'));
    }
    

    public function questions($question_set_id)
    {
        $questionSet = QuestionSet::where([['id', '=', $question_set_id], ['status', '=', 1]])->first();

        $questionSubCategoryList = QuestionSet::where([['cat_id', '=', $question_set_id], ['status', '=', 1]])->get();

        //$parentTitle = '';
        if(count($questionSubCategoryList) > 0){

            // $parentId = $questionSubCategoryList[0]->cat_id;
            // $parentDataDetails = QuestionSet::where([['id', '=', $parentId], ['status', '=', 1]])->first();

            // $parentTitle = $parentDataDetails->title;

            return view('participant.sub-test.sub_test_index', compact('question_set_id','questionSubCategoryList','questionSet'));

        } else {
            $questions = Questions::where([['question_set_id', '=', $question_set_id], ['status', '=', 1]])->get();
            return view('participant.questions', compact('question_set_id','questions','questionSet'));

        }
    }

    public function examMaterial($question_set_id)
    {
        $questionSet = QuestionSet::where([['id', '=', $question_set_id], ['status', '=', 1]])->first();
        $questionSubCategoryList = QuestionSet::where([['cat_id', '=', $question_set_id], ['status', '=', 1]])->get();

        return view('participant.sub-test.pdf_section', compact('question_set_id','questionSubCategoryList','questionSet'));
       
    }

    public function exam_submit(Request $request)
    {
        $final_score = $request->final_score;
        $final_question_set_id = $request->final_question_set_id;
        $total_question = $request->total_question_data;

        $percentage = ($final_score*100)/$total_question;

        if($percentage > 70)
        {
            $status = "Pass";
        }else{
            $status = "Fail";
        }


        $ParticipantHistory = new ParticipantHistory();
        $ParticipantHistory->user_id = Auth::user()->id;
        
        $ParticipantHistory->question_set_id = $final_question_set_id;
        $ParticipantHistory->obtained_mark = $final_score;
        $ParticipantHistory->status = $status;
        $ParticipantHistory->created_by = Auth::user()->id;
        $ParticipantHistory->updated_by = Auth::user()->id;
        $ParticipantHistory->save();

        return new JsonResponse(json_encode(array("success" => true, "message" => "Data Saved","data"=>$ParticipantHistory)));
        
       
    }

    

    public function historyForAdmin($participant_id)
    {
        
        $participant = ParticipantHistory::where('user_id','=',$participant_id)->get();
        
        $participant_details = User::where([['id', '=', $participant_id],['role_id', '=', 2], ['status', '=', 1]])->get();
        
        $participant_name = $participant_details[0]->first_name;
         return view('participant.historyForAdmin', compact('participant','participant_name'));
    }

    public function historyForParticipant()
    {
        
        $participant = ParticipantHistory::where('user_id','=',Auth::user()->id)->orderBy('id', 'DESC')->get();
        
        $participant_details = User::where([['id', '=', Auth::user()->id],['role_id', '=', 2], ['status', '=', 1]])->get();
        
        $participant_name = $participant_details[0]->first_name;
         return view('participant.historyForParticipant', compact('participant','participant_name'));
    }

}
