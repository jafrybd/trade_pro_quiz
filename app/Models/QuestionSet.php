<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionSet extends Model
{
    use HasFactory;

    public function questions()
    {
    	return $this->hasMany(Questions::class);
    }


    public function participantData()
    {
    	return $this->hasMany(ParticipantHistory::class);
    }

    public function commentOfParticipant()
    {
    	return $this->hasMany(ParticipantComment::class);
    }
}
