<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParticipantComment extends Model
{
    use HasFactory;

    public function questionSet()
    {
    	return $this->belongsTo(QuestionSet::class, 'question_set_id');
    }

    public function userDetails()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
