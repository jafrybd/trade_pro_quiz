-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2021 at 06:56 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trade_pro_quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_08_12_190913_create_question_sets_table', 2),
(5, '2021_08_19_103431_create_questions_table', 3),
(6, '2021_08_22_213432_create_participant_histories_table', 4),
(7, '2021_08_22_213929_create_participant_comments_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `participant_comments`
--

CREATE TABLE `participant_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `question_set_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participant_histories`
--

CREATE TABLE `participant_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `question_set_id` bigint(20) UNSIGNED NOT NULL,
  `obtained_mark` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `participant_histories`
--

INSERT INTO `participant_histories` (`id`, `user_id`, `question_set_id`, `obtained_mark`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 3, 8, 1, 'Fail', 3, 3, '2021-08-22 18:12:09', '2021-08-22 18:12:09'),
(2, 3, 3, 1, 'Fail', 3, 3, '2021-08-22 18:13:02', '2021-08-22 18:13:02'),
(3, 5, 3, 1, 'Fail', 5, 5, '2021-08-22 18:14:36', '2021-08-22 18:14:36'),
(4, 5, 3, 0, 'Fail', 5, 5, '2021-08-22 18:14:45', '2021-08-22 18:14:45'),
(5, 3, 8, 2, 'Fail', 3, 3, '2021-08-23 05:42:56', '2021-08-23 05:42:56'),
(6, 3, 8, 0, 'Fail', 3, 3, '2021-08-23 05:43:37', '2021-08-23 05:43:37'),
(7, 3, 8, 1, 'Fail', 3, 3, '2021-08-23 10:33:32', '2021-08-23 10:33:32'),
(8, 3, 8, 1, 'Fail', 3, 3, '2021-08-23 10:33:50', '2021-08-23 10:33:50'),
(9, 3, 8, 1, 'Fail', 3, 3, '2021-08-23 10:34:11', '2021-08-23 10:34:11'),
(10, 3, 8, 2, 'Fail', 3, 3, '2021-08-23 10:34:25', '2021-08-23 10:34:25'),
(11, 3, 8, 3, 'Pass', 3, 3, '2021-08-23 10:34:42', '2021-08-23 10:34:42'),
(12, 3, 8, 1, 'Fail', 3, 3, '2021-08-23 12:21:01', '2021-08-23 12:21:01'),
(13, 3, 8, 0, 'Fail', 3, 3, '2021-08-23 12:21:08', '2021-08-23 12:21:08'),
(14, 3, 8, 2, 'Fail', 3, 3, '2021-08-23 12:21:36', '2021-08-23 12:21:36'),
(15, 3, 8, 1, 'Fail', 3, 3, '2021-08-23 12:22:11', '2021-08-23 12:22:11'),
(16, 3, 9, 2, 'Fail', 3, 3, '2021-08-23 13:24:55', '2021-08-23 13:24:55'),
(17, 3, 9, 2, 'Fail', 3, 3, '2021-08-23 13:33:00', '2021-08-23 13:33:00'),
(18, 3, 9, 0, 'Fail', 3, 3, '2021-08-23 13:33:51', '2021-08-23 13:33:51'),
(19, 3, 9, 2, 'Fail', 3, 3, '2021-08-23 13:35:23', '2021-08-23 13:35:23'),
(20, 3, 9, 1, 'Fail', 3, 3, '2021-08-23 13:35:45', '2021-08-23 13:35:45'),
(21, 3, 9, 1, 'Fail', 3, 3, '2021-08-23 13:36:12', '2021-08-23 13:36:12'),
(22, 3, 3, 1, 'Fail', 3, 3, '2021-08-25 05:30:09', '2021-08-25 05:30:09'),
(23, 3, 10, 0, 'Fail', 3, 3, '2021-10-22 04:16:31', '2021-10-22 04:16:31'),
(24, 3, 4, 2, 'Fail', 3, 3, '2021-10-22 11:03:14', '2021-10-22 11:03:14'),
(25, 3, 6, 1, 'Fail', 3, 3, '2021-10-22 11:06:47', '2021-10-22 11:06:47'),
(26, 3, 6, 2, 'Pass', 3, 3, '2021-10-22 11:07:05', '2021-10-22 11:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('pt1@gmail.com', '$2y$10$e0j/H/iWPBclYJMh4kWeCujUauUEnpEvl81tvaaRu3cAsCLsyJWdm', '2021-10-27 08:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_set_id` bigint(20) UNSIGNED NOT NULL,
  `question_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_1_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_1_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_2_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_2_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_3_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_3_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_4_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_4_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_set_id`, `question_text`, `question_image`, `option_1_text`, `option_1_image`, `option_2_text`, `option_2_image`, `option_3_text`, `option_3_image`, `option_4_text`, `option_4_image`, `correct_answer`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(797, 10, '1.Identify the specific type of instrument calibration error shown in this graph below ?', 'B25550.jpg', 'Zero error', NULL, 'Hysteresis error', NULL, 'Linearity error', NULL, 'Span error', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(798, 10, 'In this liquid process identify the condition that could cause pressure gauge A to\nregister a greater pressure than pressure gauge B?', 'B35176.jpg', 'high rate of flow from left to right (top to bottom through the vessel)', NULL, 'extremely dense liquid', NULL, 'high rate of flow from right to left (bottom to top through the vessel)', NULL, 'Liquid boiling inside the vessel', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(799, 10, 'What would a decade box be used for?', NULL, 'To check for volts', NULL, 'To simulate volts', NULL, 'To simulate resistance', NULL, 'To measure resistance', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(800, 10, 'What is the proper procedure & care for a lanyard which has been deployed?', NULL, 'Inspect for cracks or tears & return to storage', NULL, 'Inspect for cracks or tears, document as used & return to storage', NULL, 'Document as used & return to storage', NULL, 'Cut up and dispose', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(801, 10, 'A configuration change needs to be completed to a DCS program, how would this\nbe done to assure back up for records. Using a laptop computer and proper cabling.', NULL, 'Connect laptop, make copy of program, make changes, download changes to\nprocess, verify changes, save changes to master disc, disconnect laptop.', NULL, 'Connect laptop, make changes, download changes to process, verify changes,\nsave changes to master disc, disconnect laptop.', NULL, 'Connect laptop, make copy of program, make changes, download changes to\nprocess, save changes to master disc, disconnect laptop', NULL, 'Connect laptop, put all loops on manual, make copy of program, make changes,\ndownload changes to process, verify changes, save changes to master disc,\ndisconnect laptop.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(802, 10, 'A butterfly valve appears to be having no effect on the process. The actuator is\nstroking the valve fully & valve stem is moving. What can be done to fix this problem?', NULL, 'Recalibrate the valve', NULL, 'Increase the air pressure', NULL, 'Change the actuator', NULL, 'Repair the shear pin', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(803, 10, 'A control valve , with I/P is not responding to controller signal How could the\ncalibration & operation of this valve be field checked? Assuming all approvals have been\nobtained.', NULL, 'Ensure proper air pressure, remove control signal wires, connect a signal simulator\nto the input of the VP, verify operation at 0-100% through major intervals.', NULL, 'Ensure proper air pressure, connect a signal simulator to the input of the 1/1’, verify\noperation at 0-100% through major intervals.', NULL, 'Ensure proper air pressure, connect a signal simulator to output of the VP, verify\noperation at 0-100% through major intervals.', NULL, 'Ensure maximum air pressure, remove control signal wires, connect a signal\nsimulator to output of the UP, verify operation at 0-100% through major intervals.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(804, 10, 'During a calibration check it was found that a valve with an electronic actuator is\nhunting in certain positions. What can be done to fix this?', NULL, 'Replace the current simulator', NULL, 'Fix the loose connections', NULL, 'Clean the slide wire', NULL, 'Adjust the air pressure', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(805, 10, 'How would a 4-20 ma control signal be connected from a valve to a DCS?', NULL, 'Connect the valve output to an A0 connection, configure the A0 block to connect to\nthe controller input.', NULL, 'Connect the valve output to an AI connection, configure the AI block to the\ncontroller input.', NULL, 'Connect the valve input to an A0 connection, configure the A0 block to connect to\nthe controller output.', NULL, 'Connect the valve input to an AI connection, configure the AI block to connect to the\ncontroller output.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(806, 10, 'A value on a DCS graphics screen is reading consistently wrong when compared to\nall the verified input information in the group display. What is a likely cause of this error?', NULL, 'Update time is too slow', NULL, 'Calibration of input is wrong', NULL, 'Wrong data point on graphic', NULL, 'Wrong DCS O/S revision', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(807, 10, 'Referring to the loop drawing for the Boiler Drum level, assuming all was working\nfine on auto and LT-2A lost power. (Controller does not have fail-last configuration)', 'B123303.jpg', 'LV-2A would go full open', NULL, 'LV-ZA would go full closed', NULL, 'fi-2A would give full output', NULL, 'FT-2A would give zero output', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(808, 10, 'A control system was found to be erratic & jumpy. The results were the same on\nmanual or automatic. What should be done to correct the problem?', NULL, 'Blow down the impulse lines', NULL, 'Put the controller on manual', NULL, 'Adjust the gain', NULL, 'Check the calibration', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(809, 10, 'A new pressure control loop is being commissioned. All wire & loop checks have\nbeen confirmed. On manual the output will properly open the valve to fill the vessel &\nincrease the pressure as required. While on Automatic: With a set point of 25% the\npressure eventually drains to zero and the valve ramps closed. What need to be done to\nfix this problem?', NULL, 'Calibrate the pressure transmitter', NULL, 'Increase the gain', NULL, 'Decease the gain', NULL, 'Reverse the controller', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(810, 10, 'A digital valve controller has failed, what is another device which could be used\ntemporarily to replace the DVC and still have the actuator respond to the PLC signals:', NULL, 'Flow meter', NULL, 'I/P converter', NULL, 'P/I converter', NULL, 'RTD/I converter', NULL, 2, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(811, 10, 'Referring to the water pit drawing: If the air to the bubbler was shut off, what would\nhappen?', 'B163886.jpg', 'Level would read high', NULL, 'Level control valve would open', NULL, 'Level valve would close', NULL, 'Solenoid valve would close', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(812, 10, 'A variable speed drive is calibrated: 4-20 ma input = 80Hz to 0 Hz output. The\ncontrol signal to the drive is 16 ma. What would be the measured frequency from the\ndrive?', NULL, '60Hz', NULL, '60Hz', NULL, '10Hz', NULL, '42Hz', NULL, 2, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(813, 10, 'A new thermocouple was installed into an ambient temperature process and read\n20 Deg C, when checked on the recorder. When the process was restarted and began\nheating the temperature reading gradually drifted down to the bottom of the scale of the\nrecorder. What needs to be done?', NULL, 'Reverse the wires at the thermocouple', NULL, 'Replace the thermocouple', NULL, 'Calibrate the recorder', NULL, 'Replace the Thermocouple lead wire', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(814, 10, 'How would the single point operation of an in-line nuclear density meter need to be\nverified?', NULL, 'A manometer', NULL, 'A spectrometer', NULL, 'An mv meter', NULL, 'A hydrometer', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(815, 10, 'During calibration of a Carbon Monoxide monitor, it is found that the response is\ngetting much slower than normal and the span adjustment is all the way to the high end.\nWhat should be done?', NULL, 'Nothing if calibration is fine.', NULL, 'Increase sample rate, until response improves', NULL, 'Replace chemical cell and recalibrate', NULL, 'Adjust zero up & move span down to bring in range.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(816, 10, 'A differential pressure transmitter is connected to the side of a closed water/slurry\nvessel to measure level. The high pressure leg of the transmitter has been plugging up.\nWhat can be done to help this?', NULL, 'Add an air purge to the low side', NULL, 'Add an air purge to the high side', NULL, 'Add a water purge to the low side', NULL, 'Add a water purge to the high side', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(817, 10, 'How would a vibration sensor calibration be checked?', NULL, 'Shaker table & mv meter', NULL, 'HART calibrator & current source', NULL, 'Rotary calibrator & ohm meter', NULL, 'Power supply & ma meter', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(818, 10, 'A mag flow meter is measuring a flow rate of79 L/min. The meter is calibrated for 50\nto 150 L/min. The measured ma output is 8.64 ma: What needs to be done?', NULL, 'Adjust the zero', NULL, 'Adjust the span', NULL, 'Adjust both zero & span', NULL, 'No adjustment required', NULL, 4, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(819, 10, 'A proximity sensor is being used as a speed sensor. Set up to read a split gear with\n48 teeth. How many pulses would be detected/second with a rotation of 300 RPM?', NULL, '240', NULL, '14400', NULL, '720', NULL, '480', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(820, 10, 'A process temperature, monitored with a thermocouple starts spiking from normal to\nthe “ambient” and back within a second. This is found to be an occasional problem. What\nshould be done to correct this problem?', NULL, 'Fix the broken wire at the thermocouple', NULL, 'Repair the shorted connection at the input card', NULL, 'Allow process to stabilize', NULL, 'Replace the burnt out thermocouple', NULL, 2, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(821, 10, 'The DCS reading from a pressure transmitter installed on 21250 pound steam line,\ncalibrated for 0-400 pounds, suddenly drops to zero. What should be checked first?', NULL, '24 VDC power to transmitter', NULL, 'If control is on auto', NULL, 'Impulse line to transmitter', NULL, 'DCS is scanning', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(822, 10, 'Level on an open slurry tank is being monitored using a bubble pipe and level\ntransmitter. The tank is regularly at 25% to 50% full and has been confirmed to be normal.\nThe level reading is zero %. What needs to be done?', NULL, 'Turn on the purge air', NULL, 'Calibrate the transmitter', NULL, 'Clean out the bubble pipe', NULL, 'Fill the tank', NULL, 1, 1, 1, 1, '2021-08-25 05:21:46', '2021-08-25 05:21:46'),
(823, 10, 'Diagram “A” is a descriptive diagram of a measuring device. What is this used to\nmeasure:', 'B288108.jpg', 'Pressure', NULL, 'Flow', NULL, 'Position', NULL, 'Temperature', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(824, 10, 'Using an optical sensor & a rotating disc to measure speed. Some of the reflectors\nhave become dirty, what would be indicated.', NULL, 'Normal speed', NULL, 'Slower speed', NULL, 'Faster speed', NULL, 'No speed at all', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(825, 10, 'A differential pressure reading FT251 has been reading a straight line at 0.0 % on\nthe DCS for the past four hours. Operations assures you that the flow is changing & should\nbe reading around 25%. What can be done as quick check to see if the transmitter and\ninput is responding to process change?', NULL, 'Isolate the transmitter & vent at the manifold to see if the DCS signal goes to zero', NULL, 'Disconnect the 24 V & see if the transmitter goes to zero', NULL, 'Slowly bleed the high side and reading should go upscale', NULL, 'Slowly bleed the low side and reading should go upscale', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(826, 10, 'Regarding this diagram, the positioner receives feedback from? ', 'B319455.png', ' The piston position ', NULL, 'The air signal ', NULL, 'The travel indicator ', NULL, 'The piston rotation ', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(827, 10, 'What is the best way to tune a cascade control system?', NULL, 'Tune the secondary controller first, then the primary, with the secondary controller\ntuned to be faster than the primary.', NULL, 'Tune the primary controller first then the secondary, with the secondary controller\ntuned to be slower than the primary', NULL, 'Time the primary controller first, then the secondary, with the secondary controller\ntuned to be faster than the primary.', NULL, 'Tune the secondary controller first, then the primary, with the primary controller\ntuned to be faster than the primary.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(828, 10, 'A temperature control system to a reactor using both steam for heating and chilled\nwater for cooling, uses one controller with split range valves.\n-The steam valve is ranges 12 ma (closed) to 20 ma (open)\n-The water valve is ranged 4 ma (open) to I2 ma (closed)\nWith a controller output of 14 ma what position would the valves be found?\n-0%= closed, 100% = open', NULL, 'Water 100%, steam 50%', NULL, 'Water 0%, steam 60%', NULL, 'Water 0 %, steam 25%', NULL, 'Water 62%, steam 100%', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(829, 10, 'A temperature control system to a reactor using both steam for heating and chilled\nwater for cooling, uses one controller with split range valves.\n-The steam valve is ranges 12 ma (closed) to 20 ma (open)\n-The water valve is ranged 4 ma (open) to 12 ma (closed)\nOn maximum controller signal, what position would you find the valves?', NULL, 'Steam 100 %, water 100%', NULL, 'Steam 100 %, water 0%', NULL, 'Steam 0%, water 0%', NULL, 'Steam 0%, water 100%', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(830, 10, 'A DCS network card is addressed using binary dip switches. The address is 23,\nwhich switch configuration would need to be set?', NULL, '1001001', NULL, '101011', NULL, '1010010', NULL, '10111', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(831, 10, 'A PLC program is using an address in binary 10110111. What would the switches\non the car be set at using HEX?', NULL, '7A', NULL, '57', NULL, 'B7', NULL, 'A7', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(832, 10, 'A control valve (4ma closed, 12 ma open) is 375% open, what would be the measured\nma signal to this valve?', NULL, '7', NULL, '6', NULL, '10', NULL, '5.2', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(833, 10, 'A DCS has an unused AI channel set up for a 2 wire transmitter, (meaning nothing\nis connected to it). What would have to be done if the two terminals from this channel were\nshorted?', NULL, 'Change fuse', NULL, 'Reset controller', NULL, 'Nothing', NULL, 'Replace I/O board', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(834, 10, 'A Two tower air dryer is not equalizing pressure before switching towers causing a very\nloud air discharge sound each time the towers switch from tower A to tower B. What\nshould be done to fix this problem?', NULL, 'Increase the cycle time of the dryer', NULL, 'Repair the check valves', NULL, 'Replace the desiccant', NULL, 'Change the secondary filter', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(835, 10, 'Where would a typical process alarm Annunciator panel be mounted?', NULL, 'Alarm windows top of panel, horn under floor, acknowledge buttons at arm level', NULL, 'larm windows at 3 ft. above floor, born under floor, acknowledge buttons at arm\nlevel', NULL, 'Alarm windows just above site level, horn at head level from of panel, acknowledge\nbuttons at arm level', NULL, 'Alarm windows middle of panel, horn at head level from of panel, acknowledge\nbuttons just inside the door', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(836, 10, 'A new PH probe is being put away for storage. What needs to be considered for the\nlongevity of the probe’?', NULL, 'Probe tip must be open to room air and kept cold', NULL, 'Probe up must be immersed in liquid and stored in room temperature', NULL, 'Probe up must be immersed in liquid and stored in a refrigerated area', NULL, 'Probe tip must be open to room air and kept warm', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(837, 10, 'A process gas analyzer’s sample lines are showing a buildup of sulfur. What is the\nsuggested maintenance for this problem?', NULL, 'Rod out sample lines throughout die whole length', NULL, 'Blow back lines using steam', NULL, 'Blow back lines using instrument air', NULL, 'Blow back lines using water', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(838, 10, 'A frequency converter calibrated 0-6000 Hz in and 4—20ma output , an output of\n12.53 ma. What would be the input frequency to the converter in Hz?', NULL, '3000', NULL, '3757', NULL, '2857', NULL, '3198', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(839, 10, 'A digital communicator has been connected to read a transmitters parameters.\nWhat are the 2 earliest and important parameters which can be read?', NULL, 'Serial number, range', NULL, 'Range, alarm limits', NULL, 'Tag, range', NULL, 'Location, alarm limits', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(840, 10, 'How could an unknown RTD type be confirmed using an adjustable temperature\nsource for range and calibration.', NULL, 'An ohm meter at sensing leads & resistance tables', NULL, 'An ohm meter at compensation leads & resistance tables', NULL, 'A mille-volt meter at sensing leads & millivolt tables', NULL, 'A mille-volt meter compensation leads & temperature tables', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(841, 10, 'A pyrometer reading has been drifting lower over the past 10 days, what can be\ndone to fix the problem?', NULL, 'Calibrate the DCS input', NULL, 'Clean the lens', NULL, 'Adjust the signal gain', NULL, 'Change the pyrometer', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(842, 10, 'An orifice plate connected to a Pneumatic Delta Pressure transmitter is indicating\n25% of differential range. What would be the expected transmitter output?', NULL, '5%', NULL, '75%', NULL, '25%', NULL, '50%', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(843, 10, 'An orifice plate connected to a Pneumatic Delta Pressure transmitter is indicating\n25% of differential range. What is the expected flow rate?', NULL, '5%', NULL, '75%', NULL, '25%', NULL, '50%', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(844, 10, 'What is the order of operation to isolate a D/P transmitter & a manifold on a steam\nline?', NULL, 'Close high, open low, open equalizer', NULL, 'Close high, open equalizer, close low', NULL, 'Open equalizer, close low, close high', NULL, 'Open high, open equalizer, close low', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(845, 10, 'How would an electronic delta/P transmitter be installed into an existing orifice\nplate on a steam line?', NULL, 'Low side upstream, high side downstream, at elevation of pipe', NULL, 'Low side upstream, high side downstream, below elevation of pipe', NULL, 'Low side downstream, high side upstream, below elevation of pipe', NULL, 'Low side downstream, high side upstream, at elevation of pipe', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(846, 10, 'A level transmitter is connected to an open liquid tank with an SG of 1.21.\n-The operating level of the process is measured at 150 In WC to 450 In WC.\n-That is 150 Inches of tank level = 4 ma and 20 ma = 450 inches of level.\nWhat would the transmitter be calibrated to if the transmitter was located 10 inches from\nthe bottom of the tank?', NULL, '169 in wc to 532 in wc', NULL, '150in wc to 450 in wc', NULL, '140 in wc to 440 in wc', NULL, '181 in wc to 181 in wc', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(847, 10, 'A pressure transmitter installed to measure steam is located below the steam\nheader. What needs to be considered as part of the calibration?', NULL, 'The transmitter will read slower because of the impulse line', NULL, 'The transmitter will read higher because of the impulse line', NULL, 'The transmitter will read lower because of the impulse line', NULL, 'The transmitter will read faster because of the impulse line', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(848, 10, 'A temperature transmitter converting ~50 deg C to 300 deg C, with an output of\n-10V to +10 V: What would be the expected output at 76 Deg C.', NULL, '-3.6', NULL, '-2.8', NULL, NULL, NULL, '7.5', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(849, 10, 'An open tank of water has a pressure gauge located at the bottom. The gage reads\n15 PSI. What is the height of water in the tank above the gage?', NULL, 'Less than one foot', NULL, 'More than 30 feet', NULL, 'Less than 20 feet', NULL, 'More than 50 feet', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(850, 10, 'A stack pressure transmitter, ranged 0-2 In WC, needs to be calibrated. What\nwould be the best choice to confirm the input pressure during calibration?', NULL, 'Well manometer', NULL, 'Mercury manometer', NULL, '0-2 PSI test gage', NULL, 'Incline manometer', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(851, 10, 'A standard 4-20 ma D/P transmitter is to be installed into a vacuum process which\ncalibrated at -5 PSI to 5 PSI, what would this transmitter output read when the transmitter\nhas both high & low side vented to atmosphere?', NULL, '4.00 ma', NULL, '8.00 ma', NULL, '20.00ma', NULL, '12.00 ma', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(852, 10, 'The reading across a primary fuse of a 120/24 transformer reads 120 volts. What\nneeds to be done next?', NULL, 'Replace transformer', NULL, 'Nothing everything is fine', NULL, 'Reset breaker', NULL, 'Replace fuse', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(853, 10, 'What is the first step when very fine metallic particles have been found in a\nhydraulic system oil?', NULL, 'Replace oil filters', NULL, 'Replace oil', NULL, 'Increase oil pressure', NULL, 'Replace servo valve', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(854, 10, 'A pneumatic positioner is constantly giving full output. Manually moving the flapper\ndoes not have any effect. What needs to be done?', NULL, 'reduce the air supply', NULL, 'replace the flapper', NULL, 'clean the nozzle', NULL, 'replace the valve', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(855, 10, 'When are the oilers replaced in an instrument air system?', NULL, 'When drier efficiency drops', NULL, 'Not used', NULL, 'When oil gets low', NULL, 'When tools start to stick', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(856, 10, 'An as-found reading of a linear temperature transmitter ranged 0-100 Deg C,\ndisplayed 13.5 ma with an input of 50 Deg C and 4.00 ma with an input of zero deg C.\nWhat is the next step of this calibration?', NULL, 'Tag as good and put away', NULL, 'Adjust the zero up', NULL, 'Adjust the span up', NULL, 'Adjust the span down', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(857, 10, 'An electronic input to a system is a 4-20 mA signal. The input is wired through a\n250 ohm resistor. (1-5 volts). To change the input to a 10-50 ma signal, what would the\nnew value of the resistor be in ohms?', NULL, '100', NULL, '2500', NULL, '4000', NULL, '250', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(858, 10, 'Referring to this drawing, what is the correct method to perform a zero calibration?', 'B631271.jpg', 'Close SV9, open SV1 & SV2, close SV4, and adjust sample flow', NULL, 'Open SV2, open SV4 & SV9, and adjust sample flow', NULL, 'Close SV9, open SVI & SV2, close SV8, and adjust sample flow', NULL, 'Close SV4, open SV3 & SV2, and adjust sample flow', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(859, 10, 'Referring to drawing above, how would the samples be purged?', NULL, 'Close SV10, open SV9, open SV3', NULL, 'Close SV10, open SV8, open SV 3', NULL, 'Close SV4, open SV8, open SV 3', NULL, 'Close SV4, open SV9, open SV 3', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(860, 10, 'Referring to water pit drawing: What type of valve would be required to directly\nreplace the level valve?', NULL, 'Sliding stem with electronic actuator', NULL, 'Double acting globe with electronic actuator', NULL, 'Rotating ball with electro-pneumatic actuator', NULL, 'Rotating ball with pneumatic-pneumatic actuator', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(861, 10, 'A two pair cable is being installed from a transmitter to a DCS, where would the\nshield be connected?', NULL, 'Both field ends & tape back DCS end', NULL, 'Field ends and DCS end', NULL, 'Shields are not connect at either end', NULL, 'DCS end, tape back field end', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(862, 10, 'What is the relative comparison of a PH reading of 9 compared to a PH reading of\n8? The PH 9 would be?', NULL, 'More acidic by a factor of 1', NULL, 'More basic by a factor of 1', NULL, 'More acidic by a factor of 10', NULL, 'More basic by a factor of 10', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(863, 10, 'A PH analyzer probe calibrated 0-14 = 4-20 ma, is immersed in clean water. What\nshould the approximate output be in ma?', NULL, '4', NULL, '11.97', NULL, '8.62', NULL, '19.98', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(864, 10, 'What is a brief description of a pH calibration: After loop has been put on manual.\nProper order of operations: using 4pH & 10 pH buffer solutions', NULL, 'Clean electrode, test & adjust to 4 pH, rinse electrode, test & adjust to 10pH, clean\nelectrode, test & adjust using a grab sample.', NULL, 'Clean electrode, test and adjust using a grab sample, clean electrode test & adjust\nto 4 pH, rinse electrode, test & adjust to 10pH, clean electrode', NULL, 'Test & adjust using a grab sample, clean electrode test & adjust to 4 pH, rinse\nelectrode, test & adjust to 10pH, clean electrode', NULL, 'Test & adjust to 4 pH, rinse electrode, test & adjust to 10pH, clean electrode, test\nand adjust using a grab sample.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(865, 10, 'What does a shear force constancy sensor look like?', NULL, 'Two probes with a narrow gap', NULL, 'One probe with a detector in the end', NULL, 'A spring loaded paddle', NULL, 'containment cell which the sample flows through', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(866, 10, 'How would a shear force consistency sensor be bench calibrated?', NULL, 'Using a test gas and zero with air', NULL, 'Compare to a grab sample and adjust', NULL, 'Using calculated weights', NULL, 'Using a push rod and micrometer', NULL, 3, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(867, 10, 'Regarding this diagram, what is PCV-11?', 'B721646.jpg', 'A manual pressure controller', NULL, 'A self-contained pressure control valve', NULL, 'A pneumatic control valve', NULL, 'An automatic flow control', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(868, 10, 'Regarding diagram above, what is TV-15?', NULL, 'An electronic valve, fail closed, low signal to close', NULL, 'An electro pneumatic valve, fail closed, high signal to close', NULL, 'An electronic valve, fail open, low signal to close', NULL, 'An electro pneumatic valve, fail closed, low signal to close', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(869, 10, 'Regarding the above diagram again, when the steam source shuts down, what\nwould we eventually find?', NULL, 'PCV11 would be closed, TV 15 open', NULL, 'PCV11 would be open, TV 15 open', NULL, 'PCV 11 would be open, TV 15 closed', NULL, 'PCV11 would be closed, TV 15 closed', NULL, 2, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(870, 10, 'How could the relative humidity in a room be calculated?', NULL, 'Using a sling psychrometer find wet bulb temperature & dry bulb temperature, find\nwhere the numbers intersect on a psychometric chart & read the relative humidity', NULL, 'Using a digital thermometer find wet bulb temperature & dry bulb temperature, find\nwhere the numbers intersect on a psychometric chart & read the relative humidity', NULL, 'Using a sling psychrometer find wet bulb temperature & dry bulb temperature, find\nwhere the numbers intersect on a humidity/temperature chart & read the relative\nhumidity.', NULL, 'Using a digital thermometer find wet bulb temperature & dry bulb temperature, find\nwhere the numbers intersect on a relative humidity chart & read the relative\nhumidity', NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(871, 10, 'A PLC network has 5 operator interface stations. Communications to one of the\nstations is lost, yet the other 4 are all sending and receiving data fine. Where would be the\nfirst place to check?', NULL, 'The highway controller', NULL, 'PLC control card', NULL, 'PLC Ethernet card', NULL, 'Workstation Ethernet connection', NULL, 4, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(872, 10, 'In an industrial process plant where would an area Carbon Monoxide alarm be\ninstalled?', NULL, 'Where people are normally working', NULL, 'Near the truck doors', NULL, 'In the corner of the area', NULL, NULL, NULL, 1, 1, 1, 1, '2021-08-25 05:21:47', '2021-08-25 05:21:47'),
(873, 10, 'A control valve is fitted with an actuator & electronic positioner. The valve fails\nclosed on loss of air. The positioner is calibrated 4ma open, 20 ma closed. What position\nwould you find the valve with loss of ma signal?', NULL, 'Fully open', NULL, 'Fully closed', NULL, 'As was before loss of signal', NULL, 'Depends on flow', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(874, 10, 'A new smart positioner has just been installed on an actuator/valve. During a\noperation check using a 4-20 ma signal, the valve goes either full open or full closed\nregardless of the ma signal. What is the most likely problem?', NULL, 'Low air signal', NULL, 'Wrong positioner type', NULL, 'Loose feedback arm', NULL, 'Actuator needs adjusting', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(875, 10, 'The Digital Valve controller, also known as an electronic positioner, gives an\npneumatic output to a valve. How would this be installed on a valve? After mounting DVC\non valve ….', NULL, 'Connect air supply to valve, control air signal to DVC & power supply to PLC', NULL, 'Connect air supply to DVC, control air signal to valve & ma signal to DVC output', NULL, 'Connect water lines to valve, air supply to DVC & ma signal to actuator', NULL, 'Connect air supply to DVC, control air signal to valve & ma signal to DVC input.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(876, 10, 'A reverse acting positioner installed on a fail closed valve has an input of 6.25 ma,\nwhat position would the valve be in?', NULL, '14%', NULL, '50%', NULL, '86%', NULL, '92%', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(877, 10, 'While checking the signal from a RTD/I converter it is found to work 100% while on\nthe bench. Once installed, the field calibrated output only goes to 88.2%. What needs to\nbe considered as part of the problem?', NULL, 'Input temperature is too low', NULL, 'Signal wires are too short', NULL, 'RTD is wrong type', NULL, 'Signal wires are too long', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(878, 10, 'A plug valve was found to be leaking when closed. the spring tension was increased\nto close tighter, the leak decreased significantly but did not stop. What is the next course\nof action?', NULL, 'Calibrate the positioner', NULL, 'Adjust the spring tighter', NULL, 'Repair the seat & plug', NULL, 'Repair the diaphragm', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(879, 10, 'A 4-20 ma input from a 4 wire, transmitter will be connected to a system see\ndrawing DCS input. This input will be connected to Channel 7. What terminal numbers\nwould this connection be?', NULL, '1 and 2', NULL, '14 and 15', NULL, '13 and 14', NULL, '5 and 6', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(880, 10, 'You are trying to heat a water tank to 80 deg C, which has a volume of 10m3. The\nwater tank has a constant calculated load of 8000 wat1s. (due to water flowing through the\ntank). The tank is heated with a 5000 watt heater. The tank temperature is presently 30\ndeg. What will the temperature be in 24 hours?', NULL, 'At set point & heater on', NULL, 'Below set point & heater off', NULL, 'At set point & heater off', NULL, 'Below set point & heater on', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(881, 10, 'A pneumatic/pneumatic valve positioner has stopped operating properly. The\nactuator & control signal feeding the positioner are line. What can be done in an\nemergency to get back close to working order?', NULL, 'Replace the I/P', NULL, 'Replace the positioner', NULL, 'Open the positioner by-pass', NULL, 'Increase the gain', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(882, 10, 'A new pneumatic positioner has been installed on an actuator which both are\nseparately pre-checked as working fine. This is an air-to open valve, direct acting. When\nthe supply air was mined on, the valve went fill open. Input control signal was 3PSI\nWhat is the most likely problem with the installation?', NULL, 'Input diaphragm is reversed', NULL, 'Output lines are reversed', NULL, 'Working the way it should be', NULL, 'Wrong valve for the application', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(883, 10, 'A PLC analog input card, scaled for l280O counts to 64000 counts, is connected to\n3 Linear flow signal of0~200 gal/hour. The flow rate is 150 gal/hour.\nWhat would the measured counts be?', NULL, '49300', NULL, '25600', NULL, '48000', NULL, '51200', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(884, 10, 'What equipment would be used to bench calibrate A P/I converter 3-15PSI / 4-20\nma?', NULL, 'A calibrated ma source, air supply, pressure gauge', NULL, 'DC power supply, milli-amp meter, pressure calibrator', NULL, 'Dead weight tester, voltmeter, pressure calibrator', NULL, 'Pressure calibrator, milli-volt meter, pressure gauge', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(885, 10, 'An existing pneumatic valve is being fitted with an I/P to allow interface to a new\nDCS system being installed. The area in question is considered an explosion hazard.\nFor best control and safest operation how this would be installed?\nCost is not a consideration.', NULL, 'Mount I/P in operator control room and tie into existing air line', NULL, 'Mount I/P as close as possible to valve, connect ma signal to an intrinsic barrier.', NULL, 'Mount I/P in an NEMA 4 enclosure as close as possible to valve', NULL, 'Mount I/P in DCS panel room and tie into existing air line', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(886, 10, 'What is the procedure to bench test a smart D/P flow meter?', NULL, 'Connect Pressure signal to input, power supply, & ma meter & HART communicator\nin series with the output.', NULL, 'Hart communicator will not work unless there is a minimum 250 ohm load resistor\non signal line. Connect in Parallel with device.', NULL, 'Hart communicator will not work unless there is a minimum 250 ohm load resistor\non signal line. Connect in series with device.', NULL, 'Connect Pressure signal to input, power supply, & ma meter & HART communicator\nin parallel with the output.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(887, 10, 'A new SCADA system has been installed and initialized and the network goes\ndown, what needs to be checked?', NULL, 'Node address, cables & terminating resistors of SCADA system', NULL, 'UPS, network fiber cable & HMI configuration', NULL, 'DCS configuration, tenninating resistors of SCADA system', NULL, 'HMI configuration and revision, bit parity protocols', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(888, 10, 'A DCS controller is reading data from all internal I/O, but not receiving or sending\ndata to any remote controllers, what is the most likely cause?', NULL, 'Highway controller', NULL, 'HMI server', NULL, 'Back plain', NULL, 'RS232 cable', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(889, 10, 'A DCS display is reading 75 PSI, the gage beside the PT is reading 50 PSI. The\n250 ohm input resistor gives a voltage drop of 3 volts. The input range is 0-100 PSI. What\nneeds to be done?', NULL, 'Recalibrate transmitter', NULL, 'Change the resistor', NULL, 'Recalibrate analog input card', NULL, 'Re-Range the DCS', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(890, 10, 'Sixteen digital inputs need to be installed to a PLC: They are fed from 4 separate\n110 Vac breakers. The digital input cards can handle up to 8 inputs each. What is the\nminimum number of DI cards required to meet this configuration?', NULL, '2', NULL, '3', NULL, '4', NULL, '5', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(891, 10, 'The output line of a pneumatic controller has been cut, what will happen first?', NULL, 'Valve will close', NULL, 'Set point = maximum', NULL, 'PV will increase', NULL, 'Set point = zero', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(892, 10, 'A 4-20ma/4-20 ma square root extractor has an input of 30 %, what would the\noutput be in %?', NULL, '90.10%', NULL, '5.47%', NULL, '9.90%', NULL, '54.70%', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(893, 10, 'What would the ma signal be into the square root extractor 4~20ma/4-20ma with an\noutput of 70.7%', NULL, '15.31', NULL, '5.34', NULL, '16', NULL, '12', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(894, 10, 'A direct acting controller is connected to a direct acting valve. What will an increase\nin set point result in first?', NULL, 'Error will stay the same', NULL, 'Output increase', NULL, 'Process variable decrease', NULL, 'Output decrease', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(895, 10, 'How would a stand-alone electronic controller be installed into a metal panel\nstructure?', NULL, 'Insert controller into opening, use mounting hardware to fix front of controller to\nmetal panel. Connect output signal to valve. Connect feedback signal to PV input.\nConnect 110vac power.', NULL, 'Insert controller into opening, connect output signal to flow meter, connect feedback\nsignal to valve, connect l10vac power.', NULL, 'Insert controller into opening, use mounting hardware to fix front of controller to\nmetal panel. Connect output signal to flow meter. Connect feedback signal to valve.\nConnect 110vac power.', NULL, 'Insert controller into opening, Solder controller faceplate to enclosure. Connect\noutput signal to valve. Connect feedback signal to PV input. Connect 1l0vac power.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(896, 10, 'A digital pressure transmitter capable of reading 0-40 inches of WC /4-20 ma, is\nsubjected to an input of 20 PSI, what will the output be?', NULL, '20.8', NULL, '12', NULL, '20', NULL, '4', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(897, 10, 'A valve with actuator & positioner is being checked for calibration. Results as\nfollows:\nInput valve\n0% = 0%\n25 %= 25%\n50%= 50%\n55 %=50%\n57%=50%\n75%=75%\n100%=100%\nWhat is the most likely problem?', NULL, 'Drifting calibrator', NULL, 'Low air supply', NULL, 'Leaking diaphragm', NULL, 'Dead spot on cam', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(898, 10, 'A bench set check is being done on a pneumatic spring return actuator. It was\nfound that the direction of the failure action was reversed to what was required. What is\nthe most reasonable fix?', NULL, 'Disassemble and reverse the actions of the springs', NULL, 'Replace actuator & install new one', NULL, 'Add a positioner with direction selection', NULL, 'Reverse air lines to actuator', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(899, 10, 'What is the proper sequence used to isolate the control valve, to be removed for a\nshort time of maintenance, without shutting down the process?', NULL, 'Close A, open B, close F, close E', NULL, 'Close B, open B, close F, open E', NULL, 'Open B, close E, close F, open I', NULL, 'Open B, close E, close F, open I', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(900, 10, 'A pneumatic positioner which has zero and span adjustments needs to be setup.\nHow would this calibration be done?', NULL, 'Adjust zero for minimum travel, adjust linearity for 100% travel and adjust span for\n50%, set feedback arm to 62.5% at 50% travel', NULL, 'Adjust zero for minimum travel, adjust linearity for 100% travel and adjust span for\n50%, set feedback arm to 50% at 50% travel', NULL, 'Adjust zero for minimum travel, adjust linearity for 75% travel and adjust span for\n100 %, set feedback arm to 90 deg angles at 50% travel', NULL, NULL, NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(901, 10, 'A reflectivity position sensor is starting to exhibit random false detection of objects.\nWhat can be done to fix this problem?', NULL, 'Increase the light source', NULL, 'Clean the reflector', NULL, 'Fix the alignment', NULL, 'Replace the sensor', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(902, 10, 'A PLC input from a photo detector is showing a part in-position all the time. The\nphoto detector has been replaced and problem still exists. The input indicator on the PLC\ncard is “on”. What would repair this?', NULL, 'Reset the program', NULL, 'Another new photo cell', NULL, 'Fix the broken wire', NULL, 'Fix the shorted wire', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(903, 10, 'An output signal from an electronic transmitter of 0 mA might indicate the correct\nanswer from which of the following?', NULL, 'range adjustment of the transmitter is needed.', NULL, 'No adjustment is needed.', NULL, 'process should be scaled back.', NULL, 'transmitter is malfunctioning.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(904, 10, 'In which of the following does pneumatics have a special advantage?', NULL, 'environment', NULL, 'high temperature', NULL, 'high pressure', NULL, 'explosive', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(905, 10, 'Complex numbers are used in AC circuit analysis primarily to represent which of the\nfollowing?', NULL, 'Bandwidth and harmonic content', NULL, 'Power', NULL, 'Frequency', NULL, 'Amplitude and phase shift', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(906, 10, 'Oscilloscopes can be used to do what from which of the following?', NULL, 'view the shape of voltage waveforms.', NULL, 'measure voltage over a period of time.', NULL, 'measure resistance.', NULL, 'both a and b', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(907, 10, 'When using an ammeter to measure current, which of the following is correct?', NULL, 'open circuit and connect the meter in series between the two open ends.', NULL, 'open circuit at the positive and negative terminals of the battery.', NULL, 'open circuit at one point and connect the meter to one end.', NULL, 'connect meter across the battery or load.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(908, 10, 'What must a complete test plan for system integration testing includes which of the\nfollowing?', NULL, 'Multiple test cases for each mode of operation.', NULL, 'Comments for the application programmer.', NULL, 'At least five test cases for each test.', NULL, 'Expected results for each test case.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(909, 10, 'What is the best repeatability and resolution with temperature measurement from\nwhich of the following?', NULL, 'Capillary system.', NULL, 'Resistance temperature detector (RTD).', NULL, 'Dial thermometer.', NULL, 'Thermocouple.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(910, 10, 'The valve that closes when the flow in a pipe line is reversed is the?', NULL, 'gate', NULL, 'check', NULL, 'butterfly', NULL, 'relief', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(911, 10, 'All valves cause some degree of flow loss due to friction even in the full-open\nposition. Which type of valve causes the greatest reduction in flow even when the valve\nis open?', NULL, 'gate', NULL, 'globe', NULL, 'plug valve', NULL, 'swing check valve', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(912, 10, 'Calculate the kinetic energy of an object moving at a speed of 15 meters per\nsecond, having a mass of 40 kilograms, is the correct answer given below?', NULL, '7,000 joules', NULL, '500 joules', NULL, '4,500 joules', NULL, '3,250 joules', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(913, 10, 'The latent heat of vaporization for water is which of the following given?\n', NULL, 'less than the latent heat of fusion', NULL, 'greater than the melting point', NULL, 'greater than the latent heat of fusion', NULL, 'less than the specific heat', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(914, 10, 'Which of the following calculations is correct of the voltage between test point B and\nground in this circuit?', 'B1199067.jpg', '10 volts', NULL, '16 volts', NULL, '18 volts', NULL, '12 volts', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(915, 10, 'Which of the following describes a communications cable that has a characteristic\nimpedance of 75 ohms?', NULL, 'cable should be terminated with a resistance equal to 37.5 ohms', NULL, 'cable’s conductor-to-conductor resistance is 75 ohms', NULL, 'cable’s end-to-end resistance is 75 ohms', NULL, 'cable appears to a pulse signal as a 75 ohm load', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(916, 10, 'Which of the following can cause an excessive voltage drop?', NULL, 'Too much insulation on the wire.', NULL, 'Excessively large size wires.', NULL, 'Excessive resistance in any part of the circuit.', NULL, 'short bypassing the load being tested.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(917, 10, 'Which of the following would be a possible cause of an open circuit?', NULL, 'corroded connection.', NULL, 'loose component mount.', NULL, 'pin pushed out of a connector.', NULL, 'All of the above', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(918, 10, 'Which procedure will correct the occasional discharge from a reduced pressure\nprinciple backflow preventer when the water pressure fluctuates?', NULL, 'Install a swing check valve downstream of the RP.', NULL, 'Install a swing check valve upstream of the RP.', NULL, 'Put in a stronger check valve spring upstream of the RP.', NULL, 'Restrict the water flow to the RP by using a globe valve.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(919, 10, 'Which of the following pump voltages will provide more energy efficiency?', NULL, '220 line voltage.', NULL, '240 static voltage.', NULL, '110 line voltage.', NULL, '120 static voltage.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(920, 10, 'What is the minimum distance a portable ladder is to extend above a work platform?', NULL, '36”', NULL, '28”', NULL, '12”', NULL, '24”', NULL, 1, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(921, 10, '3-phase electrical systems are favored over single-phase systems for power\ndistribution because of which of the following given below?', NULL, 'Single-phase electric motors are more rugged', NULL, '3-phase systems are easier to understand', NULL, 'expensive to convert everything to single phase', NULL, 'More power may be transmitted using less copper wire', NULL, 4, 1, 1, 1, '2021-08-25 05:21:48', '2021-08-25 05:21:48'),
(922, 10, 'An AC induction motor works on the principle of which of the following?', NULL, 'A rotating magnetic field', NULL, 'Magnetic hysteresis', NULL, 'Lorentz force', NULL, 'Voltage stepped up through mutual induction', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(923, 10, 'Which of the following are DC motors are used in industry?', NULL, 'low maintenance is necessary', NULL, 'motor’s speed must remain constant', NULL, 'motor must tolerate constant vibration', NULL, 'superior low-speed torque is needed', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(924, 10, 'Which of the following is P-type semiconductor materials are made to be the way\nthey are due to doping?', NULL, 'trivalent', NULL, 'intrinsic', NULL, 'pentavalent', NULL, 'nonvalent', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49');
INSERT INTO `questions` (`id`, `question_set_id`, `question_text`, `question_image`, `option_1_text`, `option_1_image`, `option_2_text`, `option_2_image`, `option_3_text`, `option_3_image`, `option_4_text`, `option_4_image`, `correct_answer`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(925, 10, 'Terminals designed to be fitted together and used in matching pairs are designated\nas which of the following?', NULL, 'polarized and non-polarized.', NULL, 'terminal and lug.', NULL, 'male and female.', NULL, 'terminal and connector.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(926, 10, 'Before a regulator is attached to a cylinder valve?', NULL, 'valve should be opened slightly and immediately closed', NULL, 'valve should be “cracked” wide open,', NULL, 'valve should be opened ¼ turn', NULL, NULL, NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(927, 10, 'A Reduced Pressure Principle Backflow Preventer is required when?', NULL, 'boiler feed water is thermally treated.', NULL, 'boiler feed water is from a potable source.', NULL, 'boiler feed water is from lakes or rivers.', NULL, 'boiler feed water supply is rainwater.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(928, 10, 'On a “Globe Valve”, where should the pressure enter?', NULL, 'Under the seat.', NULL, 'Depends on the desired action if the disc or plug becomes detached from the stem.(fail\nopen or fail closed)', NULL, 'Over the seat.', NULL, 'None of the above', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(929, 10, 'What is the explosive mixture range of acetylene gas in air?', NULL, '2.0% - 98%.', NULL, '3.0% - 93%.', NULL, '3.5% - 95%.', NULL, '2.5% - 80%.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(930, 10, 'The two’s complement of the binary number 11001111 is which of the following?', NULL, '110000', NULL, '11110011', NULL, '11010000', NULL, '110001', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(931, 10, 'Switch de-bouncing is a technique used for the purpose of which for the following?', NULL, 'Extending the operating life of a switch', NULL, 'Filtering noise', NULL, 'Canceling the effects of vibration', NULL, 'Eliminating false switching events', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(932, 10, 'Which of the following must poses in order for a control loop to work well under a\nwide range of conditions?', NULL, 'Negative feedback', NULL, 'Calibration drift', NULL, 'very expensive transmitter', NULL, 'Hysteresis', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(933, 10, 'If the process pressure is 1000 psi and 250°C, which of the following valve ratings\ncould be used when installing a control valve on a steam process?', NULL, 'Valve rating 1000 psi, 150°C', NULL, 'Valve rating 500 psi, 500°C', NULL, 'Valve rating 2000 psi, 100°C', NULL, 'Valve rating 2000 psi, 100°C', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(934, 10, 'What is the capacity in gallons of a water storage tank that measures 30” in\ndiameter by 80” in length?', NULL, '244 gallons', NULL, '2440 gallons', NULL, '644 gallons', NULL, '1021 gallons', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(935, 10, 'What is the symbol on the drawing below represent?', 'B1404374.jpg', 'Flanged tee outlet away from you.', NULL, 'Flanged tee outlet towards you.', NULL, 'Screwed tee outlet towards you.', NULL, 'Screwed tee side outlet away from you.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(936, 10, 'When would WHMIS require the use of a workplace label?', NULL, 'When the instructions for use are missing.', NULL, 'When a controlled product is shipped from the manufacturer.', NULL, 'When a controlled product is transferred from its’ original container.', NULL, 'When the MSDS is missing from a controlled product.', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(937, 10, 'What code governs the construction and testing of this line on a 12\" carbon steel\npipe is carrying naphtha from a distillation column to a storage tank?', NULL, 'ANSI/ASME B 31.1', NULL, 'ANSI/ASME B 31.3', NULL, 'ANSI/ASME Section VIII', NULL, 'CSA Z662', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(938, 10, 'What is the most common analog signal standard for industrial process instruments\nfrom which of the following?', NULL, '0 to 5 amps AC', NULL, '10 to 50 milliamps DC', NULL, '0 to 10 volts', NULL, '4 to 20 milliamps DC', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(939, 10, 'Which of the following does a set-point refer to in a process controller?', NULL, 'production quota for each work day', NULL, 'maximum value for the low-alarm point', NULL, 'minimum value for the high-alarm point', NULL, 'target value for the measured variable', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(940, 10, 'Which of the following parameters are the most important for calibrating a turbine\nflow-meter?', NULL, 'K-factor and frequency.', NULL, 'Strouhal number and Reynolds number.', NULL, 'Universal viscosity curve.', NULL, 'K-factor and Reynolds number.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(941, 10, 'When installing two pressure reducing valves in series, what is the recommended\nspacing between the valves?', NULL, '8ft', NULL, '10ft', NULL, '11ft', NULL, '7ft', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(942, 10, 'When installing a 400 mm carbon steel high pressure steam main, what is the\ndiameter of the drip leg required at the end of the main?', NULL, '200mm', NULL, '150mm', NULL, '250mm', NULL, '175mm', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(943, 10, 'The error tolerance of +/- 0.5% is what amount for a 4-20 mA instrument signal\ngiven from the following below?', NULL, '200mm', NULL, '150mm', NULL, '250mm', NULL, '175mm', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(944, 10, 'What is the expected output if the input pressure is 235 PSIG when a pressure\ntransmitter has a calibrated measurement range of 200 to 300 PSIG, and an output\nrange of 4-20 mA?', NULL, '9.6 mA', NULL, '3.8 mA', NULL, '17.5 mA', NULL, '6.4 mA', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(945, 10, 'What is used to calibrate the maximum range value of a blade type mechanical\nconsistency transmitter from which of the following?', NULL, 'Feedback unit.', NULL, 'Test weight.', NULL, 'Force bar.', NULL, 'Fulcrum.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(946, 10, 'What is the correct procedure to correct this issue during the completion of a punch\nlist, the engineer has sent out a revision on torque values for all flanges on the pipeline.\nAll torque values are lower than originally specified?', NULL, 'Loosen flanges and re-torque to specified value.', NULL, 'Replace gaskets and re-torque to specified value.', NULL, 'Leave flanges torqued at the higher value.', NULL, 'Send the engineer an RFI for clarification.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(947, 10, 'When filling a \npipeline for \nhydrostatic testing, \nhow should the \nvent and drain \nvalves\nbe positioned?', NULL, 'Vents open \nand drains \nare open.', NULL, 'Vents closed \nand drains \nopen.', NULL, 'Vents closed \nand drains\nclosed.', NULL, 'Vents open \nand drains \nclosed.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(948, 10, 'Which of the following is the correct answer by calculating the hydrostatic pressure\nat the bottom of a vessel holding 12 vertical feet of liquid with a density of 50 lb/ft3?', NULL, '18.867 PSI', NULL, '14.98 PSI', NULL, '5.202 PSI', NULL, '4.167 PSI', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(949, 10, 'Identify the \nthermocouple \ntype with \nthe highest \ntemperature \nlimit from \nwhich of the\nfollowing?', NULL, 'Type S', NULL, 'Type H', NULL, 'Type G', NULL, 'Type J', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(950, 10, 'When the pH \nincreases from \n3 to 4,\n what change\noccurs in \na medium?', NULL, 'becomes 10 \ntimes \nmore base.', NULL, 'becomes \n100 \ntimes \nmore acidic.', NULL, 'becomes \n100 times \nmore \nbase.', NULL, 'becomes \n10 times \nmore \nacidic.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(951, 10, 'Which of the\n following does\n the symbol\n below \nrepresent?', 'B1567682.png', 'Flange\n going \naway.', NULL, 'Tee \ncoming \ntoward \nyou.', NULL, 'Top \nof \na \nvalve.', NULL, 'Tee \ngoing \naway.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(952, 10, 'Which of the \nfollowing types \nof materials is\nnot allowed\ninside a\nboiler according\nto ASME \nSection I?', NULL, 'aluminum', NULL, 'stainless \nsteel', NULL, 'galvanized\n steel', NULL, 'carbon\n steel', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(953, 10, 'Which of the \nfollowing \nsymbols is \na controller \nlocated \nbehind a panel?', NULL, NULL, 'D1582247.png', 'E1587293.png', NULL, 'G1588290.png', NULL, NULL, 'J1586451.png', 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(954, 10, 'Which of the \nfollowing is \ncorrect when \nyou convert\n a temperature \nmeasurement of\n520o F into\n Kelvin?', NULL, '872.12 K', NULL, '799.6 K', NULL, '544.30 K', NULL, '520.10 K', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(955, 10, 'When the \nreference \njunction is \nthe same \ntemperature as \nthe measurement\n junction in a \nthermocouple \ncircuit, which \nof the following \nis the output\nvoltage \n(measured by \nthe sensing \ninstrument)?', NULL, 'Reverse \npolarity', NULL, 'Unreliable', NULL, 'Noisy', NULL, 'Zero', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(956, 10, 'What installation \nprovisions are \nnecessary to \nmaintain a \nreliable operation \nwith a local\nalarm panel\nis to be installed in \na dusty location \nwhere temperatures \nperiodically exceed \n50ºC. The \npanel\n electronics \nmust not \nexceed 45ºC?', NULL, 'dust-proof \nenclosure \nwith \nCSA approval.', NULL, 'water-proof \nenclosure \nrated to a \nclass 1 \nzone 1\n location.', NULL, 'Mount the\n enclosure \nlower than \nnormal \nwhere \ntemperature \nis lower.', NULL, 'dust-proof \nenclosure, \nwith a \ncooling \nunit or \npurged air \nsupply.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(957, 10, 'Electronic \ntransmitters \ntypically send\nout signals \nin the range \nfrom which \nof the\nfollowing?', NULL, '2-15 mA', NULL, '1-20 mA', NULL, '4-20 mA', NULL, '10-40 mA', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(958, 10, 'What is the \ncracking\n pressure \non a check \nvalve?', NULL, '2 psi', NULL, '3 to 5 psi', NULL, '9 psi', NULL, 'Above 9 psi', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(959, 10, 'Which of the \nfollowing \ndescribes \nthe meaning \nof cavitation?', NULL, 'pump outlet \nline is \noversized', NULL, 'pump is\n installed \nin a flooded \napplication', NULL, 'incomplete \nfilling of the\n pumping \nchamber', NULL, 'pump \ncan’t \nhandle \nthe load', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(960, 10, 'Flow-measuring\n elements is\n inherently \nlinear and \nrequires no \nsignal\ncharacterization \n(e.g. sqaure-root \nextraction) \nanywhere \nin the loop \nfrom which \nof the\nfollowing?', NULL, 'Orifice plate', NULL, 'Venturi', NULL, 'Turbine', NULL, 'Target', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(961, 10, 'Bernoulli’s Equation\n is a mathematical \nexpression of\n from which \nof the following?', NULL, 'Vertical height \nand pressure \nfor a static\n fluid', NULL, 'Fluid density \nand \ncompressibility \nin a restriction', NULL, 'The ratio of \nkinetic to \nviscous \nforces in a \nflow stream', NULL, 'Potential \nand \nkinetic \nenergies in a\n flow stream', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(962, 10, 'When checking a normally open relay with four terminals and an ohmmeter, no\ncontinuity will be shown between two terminals and the other two terminals will show\nwhich of the following answers given below?', NULL, 'Roughly2000 ohms.', NULL, 'Resistance somewhere between 40ohms and 120 ohms.', NULL, 'Perfect continuity (0 ohms).', NULL, 'No continuity.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(963, 10, 'Which of the\n following given \ntypes of\n directional\n control valve\n center holds \na load and \ndumps fluid\n back to tank?', NULL, 'tandem center', NULL, 'all ports closed', NULL, 'all ports open', NULL, 'floating center', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(964, 10, 'Which of the \nfollowing given\n is the correct\n method to\n choose \nhydraulic tubing?', NULL, 'Surface finish, \nOD, material', NULL, 'Material,\n OD,\n wall thickness', NULL, 'ID, \nWall thickness,\n flexibility', NULL, 'OD, \nID,\n Wall thickness', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(965, 10, 'The pH of a\n liquid solution\n is a measure \nof which of \nthe following?', NULL, 'Hydroxyl ion\n molarity', NULL, 'Dissolved \nsalt content', NULL, 'Hydrogen ion \nactivity', NULL, 'Electrical\n conductivity', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(966, 10, 'If a pH value \nis less than\n 7.0, it means \nthat the \nsolution is\n which of \nthe following?', NULL, 'Acidic', NULL, 'Conductive', NULL, 'Alkaline', NULL, 'Caustic', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(967, 10, 'When all \naccessories \nare turned off,\n the current \n(Parasitic) drain \non the battery\nshould be which \nof the following?', NULL, 'under 100\n milli-amperes.', NULL, 'under 35 \nmilli-amperes', NULL, 'zero.', NULL, 'under 3\n amperes', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(968, 10, 'In a pneumatic\n drawing, what\n does an \nequilateral \ntriangle \nrepresent?', NULL, 'Direction \nof fluid \nflow', NULL, 'Air pressure\n is \nadjustable', NULL, 'Variable \nfluid \npressure', NULL, 'GPM rating \nof the\n compressor', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(969, 10, 'During operation,\n what causes air\n pressure in a\n receiver to\n frequently \nincreases and\ndecreases?', NULL, 'MPRV setting', NULL, 'Faulty \nunloading\nvalve', NULL, 'Over heating', NULL, 'Blocked\n inlet\n filter', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(970, 10, 'Why do dual-ported\n globe valves \ntypically enjoy\n the following\n advantage over\nsingle-ported \nglobe valves?', NULL, 'Tighter shut-off', NULL, 'Easier \ndisassembly \nand\n maintenance', NULL, 'Less \nactuating\n force \nrequired', NULL, 'Longer \nservice\n life', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(971, 10, 'Which of the \nfollowing causes \ncavitation in a \ncontrol valve?', NULL, 'vibration\n in \nthe piping', NULL, 'process noise', NULL, 'Von\n Karman \neffect', NULL, 'pressure \nrecovery', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(972, 10, 'What does one\n cycle per which\n of the following\n equal to \none hertz?\n', NULL, 'second.', NULL, 'revolution', NULL, 'minute', NULL, 'any of \nthe above', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(973, 10, 'Which of the\n following given\n describes\n polarity?', NULL, 'Resistance\n of the wires', NULL, 'method of\n ground\n clamp \nfastening', NULL, 'Direction of \ncurrent flow', NULL, 'Amount of \nvolts', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(974, 10, 'What is the \nname for the\n flow of \nelectrons in an\n electric circuit?', NULL, 'Resistance', NULL, 'Capacitance', NULL, 'Voltage', NULL, 'Current', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(975, 10, 'If a pilot orifice\n is plugged, what \nwill be the\n probable \noutcome?', NULL, 'Valve will \nshuttle back \nand forth', NULL, 'Valve will \nnot open', NULL, 'Seals will \nwear out\n quickly', NULL, 'Valve will move\n past its \nhome\n position', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(976, 10, 'Which of the \nfollowing is the\n main function\n of a sequencing\n valve?', NULL, 'controls the\n speed of a\n cylinder', NULL, 'allows one\n operation to\n occur before\n another', NULL, 'regulates air\n assistance to \nthe pistons', NULL, 'dumps \nexcessive\n pressure \nfrom a \nsystem', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(977, 10, 'What is the very\n first thing you \nshould do if \nyou are the \nfirst to witness\n or discover\nan accident on \nthe job site?', NULL, 'Seek the \nassistance\n of the\n nearest \nco-worker', NULL, 'Provide\n first-aid\n to the victim', NULL, 'Alert \nprofessional\n emergency \nresponders', NULL, 'Report \nthe incident\nto your \nsupervisor', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(978, 10, 'The purpose of a \ncartridge-style \nrespirator is to\n perform which\n of the following?', NULL, 'Enhance your \npersonal \nappearance for\n maximum s\nocial appeal', NULL, 'Provide a pure \noxygen breathing\n environment \nwhere \nthere is\ninsufficient\n oxygen in the\nair', NULL, 'Convert exhaled\n carbon dioxide \nback into oxygen\n for re-breathing', NULL, 'Reduce the \nconcentration \nof particulates\n in the air \nyou breathe', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(979, 10, 'Which of the \nfollowing \nsymptoms \nindicates\n heat stroke?', NULL, 'Dizziness, \nvomiting,\n cold skin,\n profuse \nsweating', NULL, 'Hot and \ndry skin,\n inability \nto drink, \nvomiting, \nconfusion', NULL, 'Sudden \naffinity for\ncountry-western\n music', NULL, 'Cold and \nclammy skin, \nthirst, \nvomiting, \nconfusion', NULL, 2, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(980, 10, 'Which of the \nfollowing \ncauses an\n Arc blast?', NULL, 'Failure to \nlock-out\n and\n tag-out \nelectrical \nbreakers', NULL, 'Radio frequency\n emissions\n from \nhigh-power\n transmitters', NULL, 'Discharge of \nhigh electrical\n current through \nopen air', NULL, 'Poor contact \nwithin\n electrical\n wire splices', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(981, 10, 'Which procedure \nwould ensure\n equipment and\n personal safety\n when removing\n a 129 V DC \nexplosion-proof \ninstrument from \nservice?', NULL, 'Inform \noperations,\n lockout and \ntag, \nopen breaker,\n isolate \nfrom process', NULL, 'Lockout and tag,\n inform \noperations,\n open breaker,\n isolate\n from process', NULL, 'Lockout and\n tag, isolate from \nprocess, open \nbreaker, inform\n operations', NULL, 'Inform \noperations, \nisolate from \nprocess, open \nbreaker,\n lockout\n and tag', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(982, 10, 'Which of the \nfollowing is the \nmost efficient\n type of \ncompressor?', NULL, 'Multi stage', NULL, 'Single stage', NULL, 'Reciprocating', NULL, 'Single acting', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(983, 10, 'Which of the\n following is the \nnormal \natmospheric \npressure \nat sea level?', NULL, '14.7', NULL, '14.7 psia', NULL, '14.7 psig', NULL, '14.7 psi', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(984, 10, 'The sine function \nis defined as\n ratio of side \nlengths to \nwhich of the\n following?', NULL, 'Adjacent \ndivided\n by opposite', NULL, 'Opposite\n divided\n by adjacent', NULL, 'Opposite\n divided by\n hypotenuse', NULL, 'Adjacent \ndivided \nby hypotenuse', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(985, 10, 'What is the most probable cause of a 60 Hz reading being detected on the output\nsignal of an induction-type speed sensor after replacing the speed-sensing probe?', NULL, 'terminals are corroded.', NULL, 'polarity is reversed.', NULL, 'sensor is out of alignment.', NULL, 'shielding has not been properly grounded.', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(986, 10, 'Which of the following given are three good electrical conductors?', NULL, 'Copper, aluminum, paper', NULL, 'Gold, silver, wood', NULL, 'Gold, silver, aluminum', NULL, 'Copper, gold, mica', NULL, 3, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(987, 10, 'Which of the following given are four good electrical insulators?', NULL, 'Glass, air, plastic, porcelain', NULL, 'Plastic, rubber, wood, carbon', NULL, 'Paper, glass, air, aluminum', NULL, 'Glass, wood, copper, porcelain', NULL, 1, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(988, 10, 'A chemical reaction that requires a net input of energy is called which of the\nfollowing?', NULL, 'Ectomorphic', NULL, 'Catalytic', NULL, 'Spontaneous', NULL, 'Endothermic', NULL, 4, 1, 1, 1, '2021-08-25 05:21:49', '2021-08-25 05:21:49'),
(989, 10, 'Which of the following is the correct answer given if the voltage dropped across\nresistor R3 in this circuit?', NULL, '7.8 volts', NULL, '3.0 volts', NULL, '8.6 volts', NULL, '14.4 volts', NULL, 2, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(990, 10, 'Which types of transducers can be used to measure displacement or motion from\nwhich of the following given?', NULL, 'LVDTs, piezoelectric, and variable resistance.', NULL, 'Variable resistance, and thermoelectric.', NULL, 'LVDTs, variable resistance, and thermoelectric.', NULL, 'Thermoelectric, LVDTs, and sonic.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(991, 10, 'A loop diagram usually includes all of the information indicated below, except for\nwhich of the following?', NULL, 'Indication of the interrelation to other instrumentation loops, including overrides,\ninterlocks, cascaded set points, shutdowns, and safety circuits.', NULL, 'Conduit size and material in which the wires are run for the loop.', NULL, 'Wire numbers for all wires and terminal numbers for all connections depicted in the loop\ndiagram.', NULL, 'Tag numbers of instruments connected to the loop as shown on the P&IDs and depicted\nby the loop diagram.', NULL, 2, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(992, 10, 'Which of the following flow measurement devices does not require square root\nextraction?', NULL, 'Venturi flow meter', NULL, 'Orifice plate', NULL, 'Magnetic flow meter', NULL, 'Pitot tube', NULL, 3, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(993, 10, 'Proper grounding of electrical devices is important for all of the following given\nbelow reasons, except for?', NULL, 'Over-current protection', NULL, 'Electrical noise reduction', NULL, 'Heat dissipation', NULL, 'Safety', NULL, 3, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(994, 10, 'Variable frequency drives are popular in industry for electric motor control because\nof which of the following answers given below?', NULL, 'make the motor run quieter', NULL, 'cost less than an across-the-line starter', NULL, 'increase the available horsepower from a motor', NULL, 'potentially save energy', NULL, 4, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(995, 10, 'What condition in the hydraulic system causes these detrimental occurrences while\ninspecting a hydraulic system? It is noted that formation of sludge, carbon or other\ndeposits are clogging openings, causing valves and pistons to stick or leak and giving\npoor lubrication to moving parts?', NULL, 'Exposure to excessive heat and pressure.', NULL, 'Bacterial contamination.', NULL, 'Contact with oxygen absorbed in the receiver tank.', NULL, 'Excess water.', NULL, 1, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50'),
(996, 10, 'In the order\n from left to\n right, the correct\n images of the\n four control \nvalve types\nshown below?', NULL, 'Diaphragm, \nGate, \nDisc, \nGlobe', NULL, 'Ball, \nDisc, \nButterfly, \nGlobe', NULL, 'Globe, \nButterfly, \nDisc, \nBall', NULL, 'Ball, \nGate, \nButterfly, \nPlug', NULL, 4, 1, 1, 1, '2021-08-25 05:21:50', '2021-08-25 05:21:50');

-- --------------------------------------------------------

--
-- Table structure for table `question_sets`
--

CREATE TABLE `question_sets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_mark` int(11) NOT NULL DEFAULT 0,
  `duration_time` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_sets`
--

INSERT INTO `question_sets` (`id`, `cat_id`, `title`, `total_mark`, `duration_time`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 0, 'Instrumentation and Control Technician', 50, 30, 1, 1, 1, '2021-08-12 14:26:19', '2021-08-20 11:29:45'),
(4, 0, 'Industrial Electrician', 100, 60, 1, 1, 1, '2021-08-20 11:30:13', '2021-08-20 11:30:13'),
(5, 0, 'Plumber', 20, 30, 1, 1, 1, '2021-08-20 11:30:36', '2021-08-20 11:30:36'),
(6, 0, 'Construction Electrician', 10, 10, 1, 1, 1, '2021-08-20 11:30:53', '2021-08-20 11:30:53'),
(7, 0, 'Automotive Technician', 50, 25, 1, 1, 1, '2021-08-20 11:31:08', '2021-08-20 11:31:08'),
(8, 0, 'Welder', 100, 120, 1, 1, 1, '2021-08-20 11:31:53', '2021-08-20 11:31:53'),
(9, 0, 'Testing Data1', 0, 0, 1, 1, 1, '2021-08-22 18:18:36', '2021-08-23 13:40:35'),
(10, 3, 'Test 1', 10, 5, 1, 1, 1, NULL, NULL),
(11, 3, 'Test 2', 10, 5, 1, 1, 1, NULL, NULL),
(12, 3, 'Test 3', 10, 5, 1, 1, 1, NULL, NULL),
(13, 3, 'Test 4', 10, 5, 1, 1, 1, NULL, NULL),
(14, 3, 'Test 5', 10, 5, 1, 1, 1, NULL, NULL),
(15, 3, 'Test 6', 10, 5, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) NOT NULL COMMENT '1 => admin, 2 => participant',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `state`, `postal_code`, `status`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin', '', 'admin@gmail.com', '', '', '', '', 1, '2021-08-11 05:39:30', '$2y$10$mnRyQxLx415vchdEO/iPXONYTj7HVn3kOov4FXXWBfUn.Ip3iZ856', NULL, NULL, '2021-08-22 05:59:19'),
(2, 2, 'shovon', 'lal', '12345678900', 'shovon@gmail.com', '12345678900', '12345678900', '12345678900', '12345678900', 1, '2021-08-11 05:42:24', '$2y$10$3jLWgDh8Pjg7OL6huk7m8eK0Vwclr6Dd74rD1kTp4fe.kQO0.vQH6', NULL, NULL, NULL),
(3, 2, 'Akbar', 'Hossain', '01789689651', 'pt1r@gmail.com', 'Dhaka', 'CTG', 'BARI', 'MIRPUR', 1, NULL, '$2y$10$DX59fH4liIP4OkgVZ/24juDRtYDwf4sBmBGBj3IW.hxFGzzoSyIJS', 'fGwuGIWXn2fhkMHBCeksCC5OO3QYFceeZ5DtaP77meGUy6oH9WiP4493XP8j', '2021-08-19 02:40:54', '2021-10-30 11:05:33'),
(4, 2, 'Shuvo', 'khan', '01777777777', 'sj@gmail.con', 'Dhaka', 'Mirpur', 'Kafrul', '1206', 1, NULL, '$2y$10$7p8q9bUKaaN3/KdAl49iiuve1QbfGZkW.MCu3dmFeQ3UfWtcCyHWu', NULL, '2021-08-20 14:49:12', '2021-08-20 14:49:12'),
(5, 2, 'Jafry', 'Mondol', '01975006081', 'jf@gmail.com', 'Dhaka', 'Uttara', 'Sector 9', '1230', 1, NULL, '$2y$10$KUsz3Vc2LLH5D.0GofinqejgE9.OmIRkTB8Lp9gFSu5wTaih2T7Ya', NULL, '2021-08-20 14:50:29', '2021-08-20 14:50:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant_comments`
--
ALTER TABLE `participant_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participant_comments_user_id_foreign` (`user_id`),
  ADD KEY `participant_comments_question_set_id_foreign` (`question_set_id`);

--
-- Indexes for table `participant_histories`
--
ALTER TABLE `participant_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participant_histories_user_id_foreign` (`user_id`),
  ADD KEY `participant_histories_question_set_id_foreign` (`question_set_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_question_set_id_foreign` (`question_set_id`);

--
-- Indexes for table `question_sets`
--
ALTER TABLE `question_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `participant_comments`
--
ALTER TABLE `participant_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participant_histories`
--
ALTER TABLE `participant_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=997;

--
-- AUTO_INCREMENT for table `question_sets`
--
ALTER TABLE `question_sets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `participant_comments`
--
ALTER TABLE `participant_comments`
  ADD CONSTRAINT `participant_comments_question_set_id_foreign` FOREIGN KEY (`question_set_id`) REFERENCES `question_sets` (`id`),
  ADD CONSTRAINT `participant_comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `participant_histories`
--
ALTER TABLE `participant_histories`
  ADD CONSTRAINT `participant_histories_question_set_id_foreign` FOREIGN KEY (`question_set_id`) REFERENCES `question_sets` (`id`),
  ADD CONSTRAINT `participant_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_question_set_id_foreign` FOREIGN KEY (`question_set_id`) REFERENCES `question_sets` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
