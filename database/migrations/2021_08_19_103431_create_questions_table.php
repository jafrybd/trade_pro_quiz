<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('question_set_id')->constrained('question_sets');
            $table->text('question_text')->nullable();
            $table->string('question_image')->nullable();
            $table->string('option_1_text')->nullable();
            $table->string('option_1_image')->nullable();
            $table->string('option_2_text')->nullable();
            $table->string('option_2_image')->nullable();
            $table->string('option_3_text')->nullable();
            $table->string('option_3_image')->nullable();
            $table->string('option_4_text')->nullable();
            $table->string('option_4_image')->nullable();
            $table->integer('correct_answer'); // 1,2,3,4 => a,b,c,d
            $table->boolean('status')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
