!(function (e) {
  "use strict";
  var t, o, i;
  (e.avia_utilities = e.avia_utilities || {}),
    (e.avia_utilities.supported = {}),
    (e.avia_utilities.supports =
      ((t = document.createElement("div")),
      (o = ["Khtml", "Ms", "Moz", "Webkit", "O"]),
      function (e, i) {
        if (void 0 !== t.style.prop) return "";
        void 0 !== i && (o = i),
          (e = e.replace(/^[a-z]/, function (e) {
            return e.toUpperCase();
          }));
        for (var n = o.length; n--; )
          if (void 0 !== t.style[o[n] + e])
            return "-" + o[n].toLowerCase() + "-";
        return !1;
      })),
    jQuery,
    (i = "smartresize"),
    (jQuery.fn[i] = function (e) {
      return e
        ? this.bind(
            "resize",
            ((t = e),
            function () {
              var e = this,
                i = arguments;
              r ? clearTimeout(r) : n && t.apply(e, i),
                (r = setTimeout(function () {
                  n || t.apply(e, i), (r = null);
                }, o || 100));
            })
          )
        : this.trigger(i);
      var t, o, n, r;
    });
})(jQuery),
  (function (e) {
    "use strict";
    e(window).on("load", function () {
      t.load_page();
    });
    var t = {
      ready_page: function () {
        this.pre_loading(),
          this.load_page(),
          this.header_menu(),
          this.header_menu_mobile(),
          this.animate(),
          this.back_to_top(),
          this.counter(),
          this.filter_demo(),
          this.btn_scroll();
      },
      filter_demo: function () {
        jQuery(document).on("click", ".filters-demo-tabs li a", function (e) {
          e.preventDefault();
          var t = jQuery(this),
            o = t.attr("data-filter");
          t.closest(".filters-demo-tabs").find("li a").removeClass("active"),
            t.addClass("active"),
            t
              .closest(".warpper-list-demo")
              .find(".thim-demo-content")
              .isotope({ filter: o }),
            jQuery(".thim-demo-content").each(function () {
              var e = jQuery(this);
              e.css({ width: "100%" }),
                Math.floor(
                  (parseInt(e.closest(".portfolio_column").width(), 10) - 0) / 3
                ),
                e.isotope({ itemSelector: ".item", masonry: { gutter: 0 } });
            });
        });
      },
      counter: function () {
        try {
          e(".counter").counterUp({ delay: 10, time: 1e3 });
        } catch (e) {
          console.log(e);
        }
      },
      back_to_top: function () {
        var t = e("#back-to-top"),
          o = e(".site-header").outerHeight(),
          i = 100;
        e(window).on("scroll", function () {
          e(this).scrollTop() > 100
            ? t.addClass("scrolldown").removeClass("scrollup")
            : t.addClass("scrollup").removeClass("scrolldown");
        }),
          e(window).scroll(function () {
            var n = e(this).scrollTop();
            n > i && n > o
              ? t.hasClass("show_scrollup") && t.removeClass("show_scrollup")
              : t.hasClass("show_scrollup") || t.addClass("show_scrollup"),
              (i = n);
          }),
          t.on("click", function () {
            return e("html,body").animate({ scrollTop: "0px" }, 800), !1;
          });
      },
      animate: function () {},
      load_page: function () {},
      pre_loading: function () {
        try {
          e(".animsition").each(function () {
            e(this).animsition({
              inClass: "fade-in",
              outClass: "fade-out",
              inDuration: 1500,
              outDuration: 800,
              linkElement: ".animsition-link",
              loading: !0,
              loadingParentElement: "html",
              loadingClass: "pre-loading",
              loadingInner:
                '<div class="circle-border"><div class="circle-core"></div></div>',
              timeout: !1,
              timeoutCountdown: 5e3,
              onLoadEvent: !0,
              browser: ["animation-duration", "-webkit-animation-duration"],
              overlay: !1,
              overlayClass: "animsition-overlay-slide",
              overlayParentElement: "html",
              transition: function (e) {
                window.location.href = e;
              },
            });
          });
        } catch (e) {
          console.log(e);
        }
      },
      header_menu: function () {
        var t =
            e("#wrapper-container").length > 0
              ? e("#wrapper-container").offset().top
              : 0,
          o = t;
        e(window).outerWidth() <= 600 && (o = 0),
          e(window).on("resize", function () {
            (t =
              e("#wrapper-container").length > 0
                ? e("#wrapper-container").offset().top
                : 0),
              (o = t),
              e(window).outerWidth() <= 600 && (o = 0);
          });
        e("#toolbar");
        var i = e(".sticky-header"),
          n = i.find(".element-to-stick");
        if (
          (e("#toolbar").length
            ? e(".header-overlay").css({
                top: e("#toolbar").outerHeight() + "px",
              })
            : e(".header-overlay").css({ top: t + "px" }),
          n.css("top", o + "px"),
          n.length > 0)
        )
          var r = n.offset().top - o;
        e(window).on("scroll", function () {
          var t = e(this).scrollTop();
          t > r
            ? (i.css("height", i.outerHeight() + "px"), i.addClass("fixed"))
            : (i.removeClass("fixed"),
              setTimeout(function () {
                i.css("height", "auto");
              }, 200)),
            t;
        });
      },
      header_menu_mobile: function () {
        e("#primaryMenu");
        e(".menu-mobile-effect").on("click touchstart", function (t) {
          t.preventDefault(),
            e("#wrapper-container").toggleClass("mobile-menu-open");
        }),
          e(document).on(
            "click",
            ".mobile-menu-open #main-content",
            function (t) {
              t.preventDefault(),
                e("#wrapper-container.mobile-menu-open").removeClass(
                  "mobile-menu-open"
                );
            }
          );
      },
      btn_scroll: function () {
        try {
          e(".btn-scroll").on("click", function (t) {
            t.preventDefault(),
              e("html, body").animate(
                { scrollTop: e(e(this).attr("href")).offset().top },
                500
              );
          });
        } catch (e) {
          console.log(e);
        }
      },
    };
    t.ready_page();
  })(jQuery),
  (function (e) {
    e(function () {
      e("svg.radial-progress").each(function (t, o) {
        e(this).find(e("circle.complete")).removeAttr("style");
      }),
        e(window)
          .scroll(function () {
            e("svg.radial-progress").each(function (t, o) {
              e(window).scrollTop() >
                e(this).offset().top - 0.75 * e(window).height() &&
                e(window).scrollTop() <
                  e(this).offset().top +
                    e(this).height() -
                    0.25 * e(window).height() &&
                ((percent = e(o).data("percentage")),
                (radius = e(this).find(e("circle.complete")).attr("r")),
                (circumference = 2 * Math.PI * radius),
                (strokeDashOffset =
                  circumference - (percent * circumference) / 100),
                e(this)
                  .find(e("circle.complete"))
                  .animate({ "stroke-dashoffset": strokeDashOffset }, 1250));
            });
          })
          .trigger("scroll");
    });
  })(jQuery);
